-- --------------------------------------------------------
-- Host:                         scott.local
-- Server version:               5.5.36-cll-lve - MySQL Community Server (GPL) by Atomicorp
-- Server OS:                    Linux
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table sd_Fashion.admin_menu
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE IF NOT EXISTS `admin_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` char(100) NOT NULL DEFAULT '',
  `menu_url` char(100) NOT NULL DEFAULT '',
  `menu_parent` int(11) NOT NULL DEFAULT '0',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `menu_location` enum('Top','Side') NOT NULL DEFAULT 'Side',
  `menu_faicon` char(50) NOT NULL DEFAULT '',
  `menu_admin` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table sd_Fashion.admin_menu: 15 rows
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` (`menu_id`, `menu_name`, `menu_url`, `menu_parent`, `menu_order`, `menu_location`, `menu_faicon`, `menu_admin`) VALUES
	(1, 'Site', '', 0, 20, 'Side', 'home', 'No'),
	(2, 'Site View', 'site/site_view.php', 1, 20, 'Side', '', 'No'),
	(3, 'Admin', '', 0, 15, 'Top', 'gear', 'Yes'),
	(4, 'Users', 'core/user_list.php', 3, 20, 'Side', 'user', 'Yes'),
	(5, 'Menu', 'core/menu_list.php', 3, 16, 'Side', 'table', 'Yes'),
	(8, 'Modules', 'core/module_list.php', 3, 6, 'Side', 'cogs', 'Yes'),
	(9, 'Fashion', '', 0, 15, 'Side', 'camera', 'No'),
	(10, 'Events', 'fashion/event_list.php', 9, 20, 'Top', 'calendar', 'No'),
	(11, 'Sponsors', 'fashion/sponsor_list.php', 9, 15, 'Top', 'dollar', 'No'),
	(12, 'Participants', 'fashion/participant_list.php', 9, 10, 'Top', 'female', 'No'),
	(13, 'Designer', 'fashion/designer_list.php', 9, 10, 'Top', 'eye', 'No'),
	(14, 'Site Config', 'core/config_form.php', 3, 5, 'Top', 'crosshairs', 'No'),
	(15, 'Judges', 'fashion/judge_list.php', 9, 10, 'Side', 'gavel', 'No'),
	(16, 'Settings', 'fashion/config_form.php', 9, 4, 'Top', 'crosshairs', 'No'),
	(17, 'Volunteers', 'fashion/volunteer_list.php', 9, 5, 'Side', 'bullhorn', 'No');
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `config_id` int(10) NOT NULL AUTO_INCREMENT,
  `config_area` char(50) COLLATE utf16_bin DEFAULT NULL,
  `config_field` char(50) COLLATE utf16_bin DEFAULT NULL,
  `config_value` text COLLATE utf16_bin,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.config: ~17 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`config_id`, `config_area`, `config_field`, `config_value`) VALUES
	(8, 'test', 'aroo2', 'value2'),
	(9, 'test', 'aroo3', 'value3332'),
	(10, 'core', 'forgot_email', 'admin@fashion.ca'),
	(11, 'core', 'smtp_host', 'smtp.gmail.com'),
	(12, 'core', 'smtp_port', '587'),
	(13, 'core', 'smtp_security', 'tls'),
	(14, 'core', 'smtp_username', 's.davis1902@gmail.com'),
	(15, 'core', 'smtp_password', 'porsche356b'),
	(16, 'core', 'forgot_name', 'Site Admin'),
	(17, 'core', 'homepage', '5'),
	(18, 'fashion', 'judge_homepage', '<p>AFAMS judges are made up of credible men and women with proven records of integrity and accountability from the entertainment industries and their respective portfolios. They are mandated to apply the AFAMS principles of assessment accordingly in all areas of assessment. And, the decisions of the judges beyond reasonable doubt will be the one and only final report without review. However, the public vote will be highly considered by the judges as the case may be and only the telecom partners or operators may be responsible for all voting operations. AFAMS judges will be decked in pure African attires in all the events built for this project at the course of their duties. &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Application:</strong></p>\r\n\r\n<ul>\r\n	<li>All interested persons shall register online as a judge and upload his/her r&eacute;sum&eacute;, copies of credentials, passport photograph and a letter of recommendation/attestation from any legal institution.</li>\r\n</ul>\r\n\r\n<p>Qualified individuals will be contacted for interviews in their respective regions and eventually sworn in as AFAMS Judge. Our judges shall be well kept and fed from the auditions of this event to the grand finale.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&nbsp;Eligibility:</strong></p>\r\n\r\n<ul>\r\n	<li>Must be able to understand English or French with ease.</li>\r\n	<li>Must have a minimum of university education.</li>\r\n	<li>Must have a previous experience in a similar event.</li>\r\n	<li>Must have attained the level of management personnel in administration.</li>\r\n	<li>Must have a good command of oratory.</li>\r\n</ul>\r\n'),
	(19, 'fashion', 'judge_complete', '<p>Qualified individuals will be contacted for interviews in their respective regions.</p>\r\n'),
	(20, 'fashion', 'judge_upload', '<p>Please upload your&nbsp;r&eacute;sum&eacute;, copies of credentials, passport photograph and a letter of recommendation/attestation from any legal institution.&nbsp;</p>\r\n'),
	(21, 'fashion', 'volunteer_homepage', '<p>You can become a volunteer member of AFAMS by going through the free online registration process and obtain the AFAMS Volunteers slip and participate as a volunteer staff for AFAMS in any event within your region. A volunteer staff is entitled to a letter of invitation to attend all AFAMS event including the Grand finale in Barbados. AFAMS corporate authorities is determine to grant volunteers necessary protocols in procuring their visas and tickets as the case may be. The authorities are willing to appreciate volunteers on the basis of their contribution and support towards the project. However, volunteers are not entitled to remuneration or wages of any sort but will be highly appreciated and recommended for further assistance with NCC, ACTLAP and other collaborative group as the need arises.</p>\r\n\r\n<p><strong>Eligibility for Volunteers: </strong></p>\r\n\r\n<ul>\r\n	<li>Must have attained the age of 18 years and above.</li>\r\n	<li>Must be able to understand English or French language without duress.</li>\r\n	<li>Must be able to demonstrate team spirit and tolerance.</li>\r\n	<li>Must be vocal and physically fit.</li>\r\n	<li>Must have a minimum of good secondary education.</li>\r\n</ul>\r\n'),
	(22, 'fashion', 'volunteer_complete', '<p>Thank you for volunteering, some one will contact you with further details.</p>\r\n'),
	(23, 'fashion', 'volunteer_resend', '<p>Please fill in your email address. &nbsp;If a volunteer application matching your email address is found, your volunteers slip will be emailed to you.</p>\r\n'),
	(24, 'fashion', 'volunteer_resendcomplete', '<p>Please check your email. &nbsp;If&nbsp;a volunteer application matching your email address was found, then your volunteer slip will have been emailed to you.</p>\r\n');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_designer
DROP TABLE IF EXISTS `fashion_designer`;
CREATE TABLE IF NOT EXISTS `fashion_designer` (
  `designer_id` int(10) NOT NULL AUTO_INCREMENT,
  `designer_confirmed` enum('Yes','No') COLLATE utf16_bin NOT NULL DEFAULT 'No',
  `designer_name` char(200) COLLATE utf16_bin DEFAULT NULL,
  `designer_contact_firstname` char(50) COLLATE utf16_bin DEFAULT NULL,
  `designer_contact_lastname` char(50) COLLATE utf16_bin DEFAULT NULL,
  `designer_email` char(100) COLLATE utf16_bin DEFAULT NULL,
  `designer_phone` char(20) COLLATE utf16_bin DEFAULT NULL,
  `designer_password` char(64) COLLATE utf16_bin DEFAULT NULL,
  `designer_comment` text COLLATE utf16_bin,
  `designer_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `designer_username` char(50) COLLATE utf16_bin DEFAULT NULL,
  `designer_image1_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `designer_image2_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `designer_image3_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `designer_image4_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`designer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_designer: ~7 rows (approximately)
/*!40000 ALTER TABLE `fashion_designer` DISABLE KEYS */;
INSERT INTO `fashion_designer` (`designer_id`, `designer_confirmed`, `designer_name`, `designer_contact_firstname`, `designer_contact_lastname`, `designer_email`, `designer_phone`, `designer_password`, `designer_comment`, `designer_salt`, `designer_username`, `designer_image1_salt`, `designer_image2_salt`, `designer_image3_salt`, `designer_image4_salt`) VALUES
	(1, 'No', 'The best designer', 'Scott', 'Designer', 'scott@designer.com', '5558887456', '844d9f86b4602e4a58e5543cd4e740bc4bb764fc1f99a6ce3b3a0967e054b651', 'This is the comment', '385', 'scott@designer.com', NULL, NULL, NULL, NULL),
	(2, 'No', 'The big designer', 'Scotter', 'Daviser', 'sccc@asdf.com', '55544789654', '67201f1ea608aca9f92f48847071f28ba7283f008f65c79f9bbf57f9233173ba', 'Comment', 'df4', 'sccc@asdf.com', NULL, NULL, NULL, NULL),
	(3, 'No', 'Registering Designer', 'Frank', 'Zappa', 'frank@zappa.com', '7778884562', 'b3550045298b79625a3f695bc12ca858ba550dc2099fe167de7ec5babd41eb8c', 'This is my new profile, wooooo', 'b02', 'frank@zappa.com', 'de0', '8ca', 'ce2', '8f3'),
	(4, 'Yes', 'Scott', 'Scott', 'Davis', 's.davis1902@gmail.com', 'asdf', '6f18cdde26a2d8b56ea095cbea85cffe3910b26957846ca5895d4f8166e1ab8e', 'asdf', 'fd5', 'sdavis1902', '118', 'abc', '5c6', 'f2a'),
	(5, 'No', 'yfjmisdd', 'uucldglr', 'txrbgomt', 'sample@email.tst', '555-666-0606', NULL, '1', NULL, 'dpgtwgsf', NULL, NULL, NULL, NULL),
	(6, 'No', 'abrcypdn', 'fkiobyty', 'xnnoqwqy', 'sample@email.tst', '555-666-0606', NULL, '1', NULL, 'kkucvfsg', NULL, NULL, NULL, NULL),
	(7, 'No', 'juoxycqb', 'soafpclc', 'kwlppepj', 'sample@email.tst', '555-666-0606', NULL, '1', NULL, 'slmusjah', NULL, NULL, NULL, NULL),
	(8, 'No', 'juoxycqb', 'soafpclc', 'kwlppepj', 'sample@email.tst', '555-666-0606', NULL, '1', NULL, 'slmusjah', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `fashion_designer` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_designer_forgot
DROP TABLE IF EXISTS `fashion_designer_forgot`;
CREATE TABLE IF NOT EXISTS `fashion_designer_forgot` (
  `forgot_id` int(10) NOT NULL AUTO_INCREMENT,
  `forgot_code` char(64) COLLATE utf16_bin DEFAULT NULL,
  `forgot_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `forgot_date_expire` int(11) DEFAULT NULL,
  `forgot_designer_id` int(11) DEFAULT NULL,
  `forgot_link_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `forgot_link_code` char(64) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`forgot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf16 COLLATE=utf16_bin ROW_FORMAT=COMPACT;

-- Dumping data for table sd_Fashion.fashion_designer_forgot: ~23 rows (approximately)
/*!40000 ALTER TABLE `fashion_designer_forgot` DISABLE KEYS */;
INSERT INTO `fashion_designer_forgot` (`forgot_id`, `forgot_code`, `forgot_salt`, `forgot_date_expire`, `forgot_designer_id`, `forgot_link_salt`, `forgot_link_code`) VALUES
	(1, '4ee584499abe9c6e348ba7e26f000d1fd6d4588ead5927ee922ee5e60b953f4b', '05a', 1390338380, 2, '055', 'd22b931328f10da732ec0fa7ff3d3d306a5d6d710fb9650aaf3010a84b5cd12c'),
	(2, '3bb28deff0507fa95fb1248b6ec9c925d2be185bbf64d10175c8eee26e2699b3', '1c1', 1390338448, 2, 'ed9', 'f7bd74d61b0f9437102af7b4b558ca1e0697b20005d0ce913688fd8cd8a8065c'),
	(3, 'ac45ee4ff0860923b7064f0637886c65761dc18c4ac085bbc68ca13f2909dfe3', '6be', 1390339005, 2, 'f5b', '41894d9778a277c2b1f40cd69562a338b312a715ad7be7cacc50675dc772fb08'),
	(4, '96312b588910269c01dcb954b1818c8f91a2cbd1ab285c54926d063ae7ccf99a', 'aa5', 0, 2, '786', '698efab72826a43751fee0e083820f05a0753a819cd004f4353ee1e19b51e2f4'),
	(5, 'a180f792902f6841d442a9b352702c5ced661c29f6f47c5ced19af8117785a26', '338', 0, 2, '59c', '1deaa7af6cfad8c6d8e8d76f8421026d00d39f3ad704222c8bf178c93ed81ca1'),
	(6, 'cfe07ccb5c42dcab3cd1493dedc618d0ed88af0744f12c4f7394769400af0448', 'a8e', 0, 2, 'f86', '32380c5eb0aa7017c380757c6c0aad3a05f162b39c1e2f43457d6c1f9228eaa7'),
	(7, 'f59fbf5b2fb983c5593068a56ba5037e67d8509295333ca8046fe51cacc06e98', '597', 0, 2, 'eb1', 'cf343f7ab4064d584f43aa7e8854c9adfaafbf9f93423dde854dcf044e5d1336'),
	(8, 'ac2b92cfb4e8a676ace8f86d7a8994b8fc9d3f48b365e70f8f4e73a3ec579dab', 'b84', 0, 2, 'f86', 'b6334db8e9f2f65edf5fe23549afd37daedd578241bf46649fcede6b021d0565'),
	(9, 'd7534e694bee861924fbceeba647ffcf186bdcd5348ef56182453d3358666b61', '4a3', 0, 2, '8f7', '492a49b701c9d90b9a66bf9b8848238ffd45bc256085c791de88133df84c3697'),
	(10, '02f648e4108eba6fe346d94e3373c405009925ffe4159957cbc2bcaca72768c0', 'e98', 1390361271, 1, '664', 'fba9bc8646d48c6594364ce7c86f9901b521038149806912f71ff4a88f184838'),
	(11, 'ff6324157a1a9e17a13c1e3e6a098ed896ffe5bc2554cf2e70f60d843d3d72a1', '0f8', 1390361293, 1, '5fb', '8cc7f5ca0b8696965547352f7ce8e81d91c36dbb4d3ba48e72debe59a35dbc76'),
	(12, '2a09ad3c0cf822723f6e96904b4c621863a9fadc870ef2cfd488369566913616', 'f9e', 1390361398, 1, '4ca', '7b59cf7be26f428cf2755d2ec423e4fb51cc00b27c5aadad11259228afa83b9d'),
	(13, 'edede3b0afc280c382416400ca2aca1fa418016b99c663990678b0243b8fb84c', 'eb2', 1390361445, 1, 'fff', '22d6e49c11fcad60a5e62d87bf94ba7ace427617a423ece4e9f887d7bad35c5b'),
	(14, 'cf8adf90153aace86addc46ce5e8e9f063277a54b72c06bf02202ca13913a242', '964', 1390361809, 1, 'a82', '0ccf5f8b5a2dc5c77c86945cae1f6f44e7eee07ebfd19f36542212272b0d851e'),
	(15, 'ce1f81a3e6b834c85f4e2d327a293cb47723767bd4de14e244d82a0479c653de', 'b81', 1390362144, 1, 'ac7', '122936c47d54b6f1a1d25d0409df4a29014f243333e3bb8e11e4e8a7871ec3d1'),
	(16, 'b07451c95a12d75f94365b51dd0835daf95e39ac083188b4efa640692d7398b8', 'eb3', 1390515158, 0, 'fa2', '77d8e31780d4362422268a0520d5875eb0883eb53d90ef4068c6c9b9df3752fe'),
	(17, 'df994559412f6632014f4c891b6eb5fc8b8d22b0fa65a2ba0a6c41d8a745105b', '515', 1390515269, 1, '93c', '577e9c7318e475a5afd33de615a3d9720f72d310819366ce1e1ddecbeede11a5'),
	(18, '22c453f4785c6eba2a7122e4ea5dd99622096fcad42eb12c051c0ac6afb8787c', 'a24', 0, 4, 'fbc', 'bd17ccd3c4fb6dfa1754356bda6299af9d8d58db8f6f6e7264c90d811826d60e'),
	(19, '38255c3468493ca20b17abcb1b60926e690a9f1b5668672cc491d7407be5057c', 'b1d', 1392169107, 4, 'b8b', '9bc6c406cb7a05678f996e66ddbda35f9ac871bcdd4e9a29744ce7901def44a6'),
	(20, 'e50b3f881b7e136f731dc9fe924f31e6bc45101f57cd4e987fc7095540fc883a', 'ca3', 1394674208, 5, '8de', '9849723aefc0815dac297d4f0467dc355e89f64cff00f6e71cdd2af0e1e8d270'),
	(21, 'f0e3c960408471c19100d1dbe652cda60334fb81a3b3117faf334a85d4c52ad6', 'a3c', 1394720555, 5, 'ad0', '915a37ea56db85c7ee5242a842bc56fe04c024a449374867e8289dd737c5a3d7'),
	(22, '0e769d1e7a43781062e9e92f1ae6e0076b1565d572f0b73a86a7e68b95cfc484', 'cd3', 1394721071, 5, '047', 'fe719c888db88e3a77b9be327744609538b19f9dac9ee7f6986f87853d1cf79b'),
	(23, '1a9aab94e14030e06b33975f5ed33b0b936b42a204d228f2785a832b9783847b', '186', 1394721109, 5, 'cda', '872ac4745130a3295b8aa7966190cded1d3c2dcf1a4d2b77fa6ee2634e54dc9d');
/*!40000 ALTER TABLE `fashion_designer_forgot` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_event
DROP TABLE IF EXISTS `fashion_event`;
CREATE TABLE IF NOT EXISTS `fashion_event` (
  `event_id` int(10) NOT NULL AUTO_INCREMENT,
  `event_status` enum('Active','Inactive') COLLATE utf16_bin NOT NULL DEFAULT 'Active',
  `event_name` char(100) COLLATE utf16_bin DEFAULT NULL,
  `event_address` char(200) COLLATE utf16_bin DEFAULT NULL,
  `event_description` text COLLATE utf16_bin,
  `event_registration` enum('Yes','No') COLLATE utf16_bin NOT NULL DEFAULT 'No',
  `event_registration_info` text COLLATE utf16_bin,
  `event_registration_confirmation` text COLLATE utf16_bin,
  `event_date_start` int(11) DEFAULT NULL,
  `event_date_end` int(11) DEFAULT NULL,
  `event_date_registration_start` int(11) DEFAULT NULL,
  `event_date_registration_end` int(11) DEFAULT NULL,
  `event_price_participant` decimal(10,2) DEFAULT NULL,
  `event_price_designer` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_event: ~7 rows (approximately)
/*!40000 ALTER TABLE `fashion_event` DISABLE KEYS */;
INSERT INTO `fashion_event` (`event_id`, `event_status`, `event_name`, `event_address`, `event_description`, `event_registration`, `event_registration_info`, `event_registration_confirmation`, `event_date_start`, `event_date_end`, `event_date_registration_start`, `event_date_registration_end`, `event_price_participant`, `event_price_designer`) VALUES
	(1, 'Active', 'ABUJA FINALE', 'Sheraton Hotel - Abuja, Nigeria', '<p>This is a description</p>\r\n', 'Yes', '<p>For payment in Nigeria, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your ID # in the depositor&#39;s name (e.g. Joyce Michael ID# 24136)</p>\r\n', '<p>This is the registration confirmation message</p>\r\n', 1419051600, 1419224400, 1383278400, 1402545600, 50000.00, 50000.00),
	(2, 'Active', 'Abuja-Kaduna-Kano Audition', 'Immaculate Hotel, Abuja, Nigeria', '<p>This is the event description, hurray!!</p>\r\n', 'Yes', '<p>This is the message for the page where you confirm if you want to register</p>\r\n', '<p>For payment in Nigeria, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your ID # in the depositor&#39;s name (e.g. Joyce Michael ID# 24136)</p>\r\n', 1417410000, 1418187600, 1389157200, 1402545600, 10000.00, 10000.00),
	(3, 'Active', 'Accra, Ghana Audition', 'Sheraton - Accra, Ghana', '', 'Yes', '<p>Please confirm you want to register for this event by checking the box at the bottom of the page and pushing the submit button.</p>\r\n', '<p>For payment in Ghana, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP Ghana LTD<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your Registration Code ( Found at the bottom of this page ) in the depositor&#39;s name (e.g. Joyce Michael zm84)</p>\r\n', 1409544000, 1410753600, 1391230800, 1407816000, 140.00, 140.00),
	(4, 'Active', 'Benin City, Edo, Nigeria Audition', 'Emotan Hotel & Suites, Benin City, Nigeria', '', 'Yes', '<p>For payment in Nigeria, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your ID # in the depositor&#39;s name (e.g. Joyce Michael ID# 24136)</p>\r\n', '', 1416114000, 1417323600, 1391230800, 1414814400, 10000.00, 10000.00),
	(5, 'Active', 'Lagos, Nigeria Audiion', 'Ikoya, Lagos, Nigeria', '', 'Yes', '<p>For payment in Nigeria, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your ID # in the depositor&#39;s name (e.g. Joyce Michael ID# 24136)</p>\r\n', '', 1413604800, 1414728000, 1391230800, 1410235200, 10000.00, 10000.00),
	(6, 'Active', 'N\'Djamena, Chad Audition', 'Hilton - N\'Djamena, Chad', '', 'Yes', '<p>For payment in Nigeria, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your ID # in the depositor&#39;s name (e.g. Joyce Michael ID# 24136)</p>\r\n', '', 1412136000, 1413345600, 1391230800, 1411444800, 65.00, 65.00),
	(7, 'Active', 'Owerri, IMO State, Nigeria Audition', 'Sheraton Hotel, Owerri, Imo, Nigeria', '', 'Yes', '<p>For payment in Nigeria, in United States Dollars (USD) Account.<br />\r\nMake payment at:<br />\r\nAny ZENITH BANK PLC LOCATION<br />\r\nAccount Name: ACTLAP<br />\r\nAccount #: 5360011499<br />\r\nWhen making your deposit please include your ID # in the depositor&#39;s name (e.g. Joyce Michael ID# 24136)</p>\r\n', '', 1414814400, 1416027600, 1391230800, 1414555200, 10000.00, 10000.00);
/*!40000 ALTER TABLE `fashion_event` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_event_connect
DROP TABLE IF EXISTS `fashion_event_connect`;
CREATE TABLE IF NOT EXISTS `fashion_event_connect` (
  `connect_id` int(10) NOT NULL AUTO_INCREMENT,
  `connect_event_id` int(10) NOT NULL DEFAULT '0',
  `connect_participant_id` int(10) NOT NULL DEFAULT '0',
  `connect_designer_id` int(10) NOT NULL DEFAULT '0',
  `connect_requester` enum('participant','designer') COLLATE utf16_bin NOT NULL DEFAULT 'participant',
  `connect_status` enum('Pending','Accepted','Rejected') COLLATE utf16_bin NOT NULL DEFAULT 'Pending',
  `connect_message` text COLLATE utf16_bin,
  PRIMARY KEY (`connect_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_event_connect: ~1 rows (approximately)
/*!40000 ALTER TABLE `fashion_event_connect` DISABLE KEYS */;
INSERT INTO `fashion_event_connect` (`connect_id`, `connect_event_id`, `connect_participant_id`, `connect_designer_id`, `connect_requester`, `connect_status`, `connect_message`) VALUES
	(1, 1, 1, 1, 'participant', 'Accepted', 'I would like to connect with you, your stuff looks super');
/*!40000 ALTER TABLE `fashion_event_connect` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_event_register
DROP TABLE IF EXISTS `fashion_event_register`;
CREATE TABLE IF NOT EXISTS `fashion_event_register` (
  `register_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `register_event_id` int(10) unsigned DEFAULT NULL,
  `register_checked_in` char(3) COLLATE utf16_bin NOT NULL DEFAULT 'No',
  `register_date_created` int(10) DEFAULT NULL,
  `register_code` varchar(40) COLLATE utf16_bin DEFAULT NULL,
  `register_participant_id` int(10) unsigned DEFAULT NULL,
  `register_designer_id` int(10) unsigned DEFAULT NULL,
  `register_price` decimal(14,2) DEFAULT NULL,
  `register_amount_paid` decimal(14,2) DEFAULT NULL,
  `register_paid` char(3) COLLATE utf16_bin NOT NULL DEFAULT 'No',
  PRIMARY KEY (`register_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_event_register: ~10 rows (approximately)
/*!40000 ALTER TABLE `fashion_event_register` DISABLE KEYS */;
INSERT INTO `fashion_event_register` (`register_id`, `register_event_id`, `register_checked_in`, `register_date_created`, `register_code`, `register_participant_id`, `register_designer_id`, `register_price`, `register_amount_paid`, `register_paid`) VALUES
	(1, 1, 'No', NULL, NULL, 1, NULL, 11.00, 11.00, 'Yes'),
	(2, 1, 'No', NULL, NULL, NULL, 1, 11.00, 11.00, 'Yes'),
	(3, 1, 'No', NULL, NULL, NULL, 2, 11.00, 11.00, 'Yes'),
	(4, 1, 'No', NULL, NULL, NULL, 3, 11.00, 11.00, 'Yes'),
	(5, 1, 'No', 1391881712, '', 8, 0, 10.00, NULL, 'Yes'),
	(6, 1, 'No', 1391882636, '', 0, 4, 5.00, NULL, 'Yes'),
	(7, 3, 'No', 1392169218, '', 0, 4, 140.00, 140.00, 'Yes'),
	(8, 3, 'No', 1392770635, '', 8, 0, 140.00, NULL, 'No'),
	(9, 6, 'No', 1392770823, '', 8, 0, 65.00, NULL, 'No'),
	(10, 2, 'No', 1392772379, '', 8, 0, 10000.00, NULL, 'No');
/*!40000 ALTER TABLE `fashion_event_register` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_judge
DROP TABLE IF EXISTS `fashion_judge`;
CREATE TABLE IF NOT EXISTS `fashion_judge` (
  `judge_id` int(11) NOT NULL AUTO_INCREMENT,
  `judge_firstname` char(100) COLLATE utf16_bin DEFAULT NULL,
  `judge_lastname` char(100) COLLATE utf16_bin DEFAULT NULL,
  `judge_gender` enum('Male','Female') COLLATE utf16_bin DEFAULT 'Male',
  `judge_date_birth` int(11) DEFAULT NULL,
  `judge_email` char(100) COLLATE utf16_bin DEFAULT NULL,
  `judge_phone` char(100) COLLATE utf16_bin DEFAULT NULL,
  `judge_address` char(200) COLLATE utf16_bin DEFAULT NULL,
  `judge_comment` text COLLATE utf16_bin,
  `judge_approved` char(3) COLLATE utf16_bin DEFAULT 'No',
  `judge_complete` char(3) COLLATE utf16_bin DEFAULT 'No',
  `judge_date_application` int(11) DEFAULT NULL,
  PRIMARY KEY (`judge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_judge: ~3 rows (approximately)
/*!40000 ALTER TABLE `fashion_judge` DISABLE KEYS */;
INSERT INTO `fashion_judge` (`judge_id`, `judge_firstname`, `judge_lastname`, `judge_gender`, `judge_date_birth`, `judge_email`, `judge_phone`, `judge_address`, `judge_comment`, `judge_approved`, `judge_complete`, `judge_date_application`) VALUES
	(1, 'Scottjudge', 'Judger', 'Male', 1399953600, 's_davis1902@hotmail.com', '5165007759', '100 Tideland Drive', 'This is my comment', 'No', 'Yes', 1399130142),
	(2, 'Judge2', 'name', 'Male', 1399262400, 's_davis1902@hotmail.com', '5165007759', '100 Tideland Drive', 'sdasdf', 'No', 'Yes', 1399130253),
	(3, 'Frank', 'someguy', 'Male', 1398916800, 'frank@someguy.com', '5554786654', 'some address', 'this is my comment', 'No', 'Yes', 1399137986);
/*!40000 ALTER TABLE `fashion_judge` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_judge_file
DROP TABLE IF EXISTS `fashion_judge_file`;
CREATE TABLE IF NOT EXISTS `fashion_judge_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_judge_id` int(11) NOT NULL DEFAULT '0',
  `file_extension` char(4) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `file_salt` char(3) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `file_name` char(100) COLLATE utf16_bin DEFAULT NULL,
  `file_type` char(150) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_judge_file: ~6 rows (approximately)
/*!40000 ALTER TABLE `fashion_judge_file` DISABLE KEYS */;
INSERT INTO `fashion_judge_file` (`file_id`, `file_judge_id`, `file_extension`, `file_salt`, `file_name`, `file_type`) VALUES
	(1, 1, 'pdf', '396', 'First Test File', NULL),
	(3, 1, 'docx', '0', 'Word Doc', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	(7, 1, 'docx', '468', 'Resume', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
	(8, 2, 'png', '646', 'afsd', 'image/png'),
	(9, 3, 'xlsx', '89c', 'my excel file', 'application/zip'),
	(10, 3, 'pdf', '713', 'my pdf', 'application/pdf');
/*!40000 ALTER TABLE `fashion_judge_file` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_participant
DROP TABLE IF EXISTS `fashion_participant`;
CREATE TABLE IF NOT EXISTS `fashion_participant` (
  `participant_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_confirmed` enum('Yes','No') COLLATE utf16_bin NOT NULL DEFAULT 'No',
  `participant_firstname` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_lastname` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_image1_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `participant_image2_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `participant_image3_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `participant_image4_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `participant_identification_type` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_identification_number` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_pet_name` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_birth_place` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `participant_date_birth` int(10) DEFAULT NULL,
  `participant_state_origin` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_lga_origin` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_nationality` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_blood_type` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_gender` enum('Male','Female') COLLATE utf16_bin NOT NULL DEFAULT 'Female',
  `participant_skin_colour` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_marital_status` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_height` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_weight` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_address` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `participant_email` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `participant_phone` varchar(20) COLLATE utf16_bin DEFAULT NULL,
  `participant_last_school` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_certificate_obtained` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `participant_philosophy` text COLLATE utf16_bin,
  `participant_belief` text COLLATE utf16_bin,
  `participant_role_model` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `participant_closest_friend` varchar(100) COLLATE utf16_bin DEFAULT NULL,
  `participant_favourite_colour` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `participant_best_game` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `participant_favourite_music` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `participant_favourite_movie` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `participant_favourite_model` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `participant_password` varchar(64) COLLATE utf16_bin DEFAULT NULL,
  `participant_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `participant_username` char(50) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`participant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_participant: ~14 rows (approximately)
/*!40000 ALTER TABLE `fashion_participant` DISABLE KEYS */;
INSERT INTO `fashion_participant` (`participant_id`, `participant_confirmed`, `participant_firstname`, `participant_lastname`, `participant_image1_salt`, `participant_image2_salt`, `participant_image3_salt`, `participant_image4_salt`, `participant_identification_type`, `participant_identification_number`, `participant_pet_name`, `participant_birth_place`, `participant_date_birth`, `participant_state_origin`, `participant_lga_origin`, `participant_nationality`, `participant_blood_type`, `participant_gender`, `participant_skin_colour`, `participant_marital_status`, `participant_height`, `participant_weight`, `participant_address`, `participant_email`, `participant_phone`, `participant_last_school`, `participant_certificate_obtained`, `participant_philosophy`, `participant_belief`, `participant_role_model`, `participant_closest_friend`, `participant_favourite_colour`, `participant_best_game`, `participant_favourite_music`, `participant_favourite_movie`, `participant_favourite_model`, `participant_password`, `participant_salt`, `participant_username`) VALUES
	(1, 'No', 'Scott', 'Davis', 'b62', 'd85', 'c4a', 'feb', 'ID', '1234', 'The dude', NULL, 952405200, 'Ontario', 'Whaaaa', 'Canadian', 'Dunnno', 'Male', 'White', 'Married', '5 foot 11', '100 pounds', '100 Tideland Dr, Brampton, Ontario, Canada', 's_davis1902@hotmail.com', '5554441122', 'Some School', 'Some certificate', 'I don\'t really have a philosophy', 'I am a Christian.', 'Christ', 'Doosuur', 'Dunno', 'Dunno', 'Classic Rock', 'Shawshank Redemption', 'Don\'t have one', 'be6bc5bae0998e4eaed1e1f1a06acb8e169697c5c53b3d6dbb5ddc592ea3c0c0', 'dd9', 's_davis1902@hotmail.com'),
	(8, 'Yes', 'Scott', 'Davis', '1ee', '0c5', 'd60', '5a0', 'jklh', 'hjkl', 'kjhl', NULL, 1391403600, 'ON - Ontario', 'hgjk', 'jghk', 'jhkg', 'Male', 'jhgk', 'jghk', 'gjhk', 'kghj', '100 Tideland Drive', 's.davis1902@gmail.com', '5165007759', 'hgkj', 'khgj', 'hkgj', 'jghk', 'jkgh', 'jkhg', 'ghjk', 'ghkj', 'hgjk', 'hjgk', 'ghkj', '0f5d76dfe5eb0ad986fd7be457b7f4558ca318903348682cdf04ba668efaba78', 'ef6', 'sdavis1902'),
	(9, 'No', 'jsfuomky', 'oyiigxfn', NULL, NULL, NULL, NULL, '1', '1', 'ewrfdhtq', NULL, -94676400, 'NY', '1', '1', '1', 'Male', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'psshlkyu', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'mjwgmqhn'),
	(10, 'No', 'wfedxusw', 'lgjegrew', NULL, NULL, NULL, NULL, '1', '1', 'bgpuawdu', NULL, -94676400, 'NY', '1', '1', '1', 'Female', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'bjdasemb', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'oyngoxfn'),
	(11, 'No', 'kkufroki', 'lfpgasgy', NULL, NULL, NULL, NULL, '1', '1', 'hgavbruh', NULL, -94676400, 'NY', '1', '1', '1', 'Male', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'ujesxmky', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'nxyvtkfg'),
	(12, 'No', 'gsxlpqtr', 'vbpnapxo', NULL, NULL, NULL, NULL, '1', '1', 'hoyawwie', NULL, -94676400, 'NY', '1', '1', '1', 'Female', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'hqylwurp', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'rrtgsykb'),
	(13, 'No', 'kkufroki', 'lfpgasgy', NULL, NULL, NULL, NULL, '1', '1', 'hgavbruh', NULL, -94676400, 'NY', '1', '1', '1', 'Male', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'ujesxmky', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'nxyvtkfg'),
	(14, 'No', 'gsxlpqtr', 'vbpnapxo', NULL, NULL, NULL, NULL, '1', '1', 'hoyawwie', NULL, -94676400, 'NY', '1', '1', '1', 'Female', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'hqylwurp', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'rrtgsykb'),
	(15, 'No', 'kkufroki', 'lfpgasgy', NULL, NULL, NULL, NULL, '1', '1', 'hgavbruh', NULL, -94676400, 'NY', '1', '1', '1', 'Male', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'ujesxmky', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'nxyvtkfg'),
	(16, 'No', 'gsxlpqtr', 'vbpnapxo', NULL, NULL, NULL, NULL, '1', '1', 'hoyawwie', NULL, -94676400, 'NY', '1', '1', '1', 'Female', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'hqylwurp', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'rrtgsykb'),
	(17, 'No', 'xjmghwly', 'pjoajiyo', NULL, NULL, NULL, NULL, '1', '1', 'fmapitsv', NULL, -94676400, 'NY', '1', '1', '1', 'Male', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'bunylmad', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'shxyiayc'),
	(18, 'No', 'ipjuibup', 'cqfexndc', NULL, NULL, NULL, NULL, '1', '1', 'hvwelare', NULL, -94676400, 'NY', '1', '1', '1', 'Female', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'ktewcwyq', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'qrwrhksp'),
	(19, 'No', 'xjmghwly', 'pjoajiyo', NULL, NULL, NULL, NULL, '1', '1', 'fmapitsv', NULL, -94676400, 'NY', '1', '1', '1', 'Male', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'bunylmad', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'shxyiayc'),
	(20, 'No', 'ipjuibup', 'cqfexndc', NULL, NULL, NULL, NULL, '1', '1', 'hvwelare', NULL, -94676400, 'NY', '1', '1', '1', 'Female', '1', '1', '1', '1', '3137 Laguna Street', 'sample@email.tst', '555-666-0606', 'ktewcwyq', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'qrwrhksp');
/*!40000 ALTER TABLE `fashion_participant` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_participant_forgot
DROP TABLE IF EXISTS `fashion_participant_forgot`;
CREATE TABLE IF NOT EXISTS `fashion_participant_forgot` (
  `forgot_id` int(10) NOT NULL AUTO_INCREMENT,
  `forgot_code` char(64) COLLATE utf16_bin DEFAULT NULL,
  `forgot_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `forgot_date_expire` int(11) DEFAULT NULL,
  `forgot_participant_id` int(11) DEFAULT NULL,
  `forgot_link_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `forgot_link_code` char(64) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`forgot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf16 COLLATE=utf16_bin ROW_FORMAT=COMPACT;

-- Dumping data for table sd_Fashion.fashion_participant_forgot: ~27 rows (approximately)
/*!40000 ALTER TABLE `fashion_participant_forgot` DISABLE KEYS */;
INSERT INTO `fashion_participant_forgot` (`forgot_id`, `forgot_code`, `forgot_salt`, `forgot_date_expire`, `forgot_participant_id`, `forgot_link_salt`, `forgot_link_code`) VALUES
	(1, '4ee584499abe9c6e348ba7e26f000d1fd6d4588ead5927ee922ee5e60b953f4b', '05a', 1390338380, 2, '055', 'd22b931328f10da732ec0fa7ff3d3d306a5d6d710fb9650aaf3010a84b5cd12c'),
	(2, '3bb28deff0507fa95fb1248b6ec9c925d2be185bbf64d10175c8eee26e2699b3', '1c1', 1390338448, 2, 'ed9', 'f7bd74d61b0f9437102af7b4b558ca1e0697b20005d0ce913688fd8cd8a8065c'),
	(3, 'ac45ee4ff0860923b7064f0637886c65761dc18c4ac085bbc68ca13f2909dfe3', '6be', 1390339005, 2, 'f5b', '41894d9778a277c2b1f40cd69562a338b312a715ad7be7cacc50675dc772fb08'),
	(4, '96312b588910269c01dcb954b1818c8f91a2cbd1ab285c54926d063ae7ccf99a', 'aa5', 0, 2, '786', '698efab72826a43751fee0e083820f05a0753a819cd004f4353ee1e19b51e2f4'),
	(5, 'a180f792902f6841d442a9b352702c5ced661c29f6f47c5ced19af8117785a26', '338', 0, 2, '59c', '1deaa7af6cfad8c6d8e8d76f8421026d00d39f3ad704222c8bf178c93ed81ca1'),
	(6, 'cfe07ccb5c42dcab3cd1493dedc618d0ed88af0744f12c4f7394769400af0448', 'a8e', 0, 2, 'f86', '32380c5eb0aa7017c380757c6c0aad3a05f162b39c1e2f43457d6c1f9228eaa7'),
	(7, 'f59fbf5b2fb983c5593068a56ba5037e67d8509295333ca8046fe51cacc06e98', '597', 0, 2, 'eb1', 'cf343f7ab4064d584f43aa7e8854c9adfaafbf9f93423dde854dcf044e5d1336'),
	(8, 'ac2b92cfb4e8a676ace8f86d7a8994b8fc9d3f48b365e70f8f4e73a3ec579dab', 'b84', 0, 2, 'f86', 'b6334db8e9f2f65edf5fe23549afd37daedd578241bf46649fcede6b021d0565'),
	(9, 'd7534e694bee861924fbceeba647ffcf186bdcd5348ef56182453d3358666b61', '4a3', 0, 2, '8f7', '492a49b701c9d90b9a66bf9b8848238ffd45bc256085c791de88133df84c3697'),
	(10, '02f648e4108eba6fe346d94e3373c405009925ffe4159957cbc2bcaca72768c0', 'e98', 1390361271, 1, '664', 'fba9bc8646d48c6594364ce7c86f9901b521038149806912f71ff4a88f184838'),
	(11, 'ff6324157a1a9e17a13c1e3e6a098ed896ffe5bc2554cf2e70f60d843d3d72a1', '0f8', 1390361293, 1, '5fb', '8cc7f5ca0b8696965547352f7ce8e81d91c36dbb4d3ba48e72debe59a35dbc76'),
	(12, '2a09ad3c0cf822723f6e96904b4c621863a9fadc870ef2cfd488369566913616', 'f9e', 1390361398, 1, '4ca', '7b59cf7be26f428cf2755d2ec423e4fb51cc00b27c5aadad11259228afa83b9d'),
	(13, 'edede3b0afc280c382416400ca2aca1fa418016b99c663990678b0243b8fb84c', 'eb2', 1390361445, 1, 'fff', '22d6e49c11fcad60a5e62d87bf94ba7ace427617a423ece4e9f887d7bad35c5b'),
	(14, 'cf8adf90153aace86addc46ce5e8e9f063277a54b72c06bf02202ca13913a242', '964', 1390361809, 1, 'a82', '0ccf5f8b5a2dc5c77c86945cae1f6f44e7eee07ebfd19f36542212272b0d851e'),
	(15, 'ce1f81a3e6b834c85f4e2d327a293cb47723767bd4de14e244d82a0479c653de', 'b81', 1390362144, 1, 'ac7', '122936c47d54b6f1a1d25d0409df4a29014f243333e3bb8e11e4e8a7871ec3d1'),
	(16, 'b07451c95a12d75f94365b51dd0835daf95e39ac083188b4efa640692d7398b8', 'eb3', 1390515158, 0, 'fa2', '77d8e31780d4362422268a0520d5875eb0883eb53d90ef4068c6c9b9df3752fe'),
	(17, 'df994559412f6632014f4c891b6eb5fc8b8d22b0fa65a2ba0a6c41d8a745105b', '515', 1390515269, 1, '93c', '577e9c7318e475a5afd33de615a3d9720f72d310819366ce1e1ddecbeede11a5'),
	(18, 'ddc3c9a5aa8ecf48d7d271710b48451e6a241652be4f72cfde57ee2f29e1ca04', '25d', 1390869094, NULL, '302', '32c10f19df55aeccce878683e4c767ab50de6e6a6bd93bc015950823765a49e6'),
	(19, '60e856d339964cef3d6c091a82575c0177da8ab27ea6426036d1e0bd4055b51a', '75d', 0, 1, '707', '220ad5c7a4a5d64e9c5e4a4bca91a39d60ec23c741aad958e9b36199757e090a'),
	(20, 'e151e931d1c9f6e967fcd8f7f5a17cb1f3306d8ed9d9506ea4a47e1a120d47ed', 'd24', 0, 1, '35b', 'b3fd513bc8f8b78a681a4d552d5e495c24cd5fb79c07d8d9bc1994d5d3d08747'),
	(21, '5105cb42c9d77ab8c6977be95de5d188ad3e8de4d3913b86913d29073eb7963e', '481', 1391827371, 1, '126', 'd40b14b0f3c45a2ea5379e0d32e455f0f41edf826ffc7e8c7651b8fc15bfd32e'),
	(22, 'd38248d544d0afbc482c7fd0b6aad2ec6d1d2ac5e995f007593d3e843f3a1c0e', '981', 1391878161, 0, 'ef3', 'c1e1405e9b3e859bf5ef991fe56479f2c81c5106e10ea01d17eb2e76e716a18a'),
	(23, 'a8983cc0016595524245f6522a14e42d70f2ceedca4a6de5cbce9beea215955e', '810', 1391878452, 0, 'd34', '1c9cd2c8a369e01c68b19b76dc67a75a81be01a34163fe1ea66700d69881eeda'),
	(24, '874cd952036ee13f6ef559b13c0bf7ea442c9b16026c9854b9097c30b7a236d1', 'b00', 1391879553, 4, '8f5', '2abc604aaeddf21b6e8a01441347b63a92b460eb64d86018a4d7e1fc4d7f9c89'),
	(25, '03fc4e1da1324177441eadb7998df54dc2e60a382bcae07f71844518685372a5', '8dc', 0, 5, '830', 'e7cbab7f8eb00f1be874ffb70760cd96dfab9d7162076dd73e70e20c8aefeeab'),
	(26, '1b75100c765b8c3dd6df8a1070a8da6c30b96ab280c8c3ff97f2f181792076b8', 'dc6', 0, 6, 'e42', '97982356f794c62f41360ed9f3d9b27525da7c4063a0862efa2dc21a95a28594'),
	(27, '4d4370c6f6b9356362bed20cf853516eb79ce1c4ef63d6dd222a6783496badd0', 'b24', 0, 8, 'b41', '1c2c9b7fa8695550ccf2a014c61d59a3010c624841325adfef18e916edd8f327'),
	(28, 'd53d57b7b26a57209c646562dcdfcba9b8b7ffa51f793f101bbfa8a453688bbf', '601', 1394674194, 9, '92b', '0a2557ad00a14f602e95f3f47d3962c8a02da23d81d3ac77469a03a44a2999d4'),
	(29, '26894153fd81bb7fed61631096fd21eb64b22492a2414b60a88f3eb35974e519', '430', 1394674204, 9, '08c', 'e677c9f9b5cd3d8af7d279e6cded507e558599a6dbe4470b8a6a4a579a5f1cb6'),
	(30, 'cfcd715bf25edbb217db8e467fa1164e6bce06cf37e81bb0ae1c70620ffa4915', 'f0e', 1394720581, 9, 'cd0', '5546ef4ce7ba518cf315136f03506347ffae638dae1a3ddf34a6f94f519f2921'),
	(31, 'a5fc4a93e594d08e7265a2127d1c0edef552ab91986bf6ea5a2318128ec60f59', '43d', 1394720597, 9, '319', 'da692bd869d8d54b9ddc853aef32831a183e19b1301a2d56c383ecf1492e6b60'),
	(32, '5b3b044014e93761987d1ec72ead74fc65f5358507819d10a2db9663468f2293', '727', 1394720615, 9, '2e1', 'c8b0c7a0abafb096c3d691d4e3cf8dcf738dd29512bbb3f6fe3b08c71c95305a'),
	(33, 'a854a214ed4efd8cd5f0c42fb13885b40a91073c005a9e009e97f115198d68a7', 'cb7', 1394720629, 9, '7a3', '15c4c25a1bdae8a67151a9db9338c107b28eb43febb41799053a8b2c9aa0fbe1'),
	(34, 'e262c41668bd293f98daaf7b642bc8611ab2b9c2a97e7bb93345a46ae610299a', '09e', 1394720630, 9, '6a2', '5608148947c3a48304673bc4fe37f9c9c5225c1e1191a596efb1540cd74e10be'),
	(35, '5a4769ac57db82c6c81447378eb279d27a638e5976416600379b77c423b8b4e7', '26f', 1394720632, 9, '87b', '98b468f34996075596cfe54b148d22fe7256e48b8d85b7ed42ea5943a7492d17'),
	(36, '3f84b01ce80af9ce6c729e859240fd2599d1a0cad76c858708ab88680b22e151', 'd5c', 1394721101, 9, 'b5b', '0ffabd956e5e59ebd9531149242958aaaf52081fed07f126c821add4446ed077'),
	(37, '831e84602b3a667c6907b47a51d4f14131c4deeec1754a48c5abeb9ff60eb4db', '740', 1394721105, 9, '901', '08cf0bab107775b3f0b73a4ce9fe950c7ad6d843dbf36cc306a1bc106f5140f5'),
	(38, 'f25f8a66140a54d9cb3184b529fb8131757f1ae176cbd6f36febdc0602665bb5', '828', 1394721111, 9, 'f08', 'db5f022ee89a1ee25ab0b11a6cf7b3931cf6c524075c926c414855bbffa3580c'),
	(39, '72eabf0f322397c25f0bfc161c455cefc2cc0351ffe97e2e13f540b3ee0b1f2c', '37b', 1394721113, 9, '531', 'dfa1f7dd1dfaa6bb3bbf0a64a7e088a748ac1d0c66c04971665eb933b7fce7ef');
/*!40000 ALTER TABLE `fashion_participant_forgot` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_sponsor
DROP TABLE IF EXISTS `fashion_sponsor`;
CREATE TABLE IF NOT EXISTS `fashion_sponsor` (
  `sponsor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sponsor_name` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `sponsor_contact_firstname` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `sponsor_contact_lastname` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `sponsor_email` varchar(60) COLLATE utf16_bin DEFAULT NULL,
  `sponsor_phone` varchar(20) COLLATE utf16_bin DEFAULT NULL,
  `sponsor_address` varchar(200) COLLATE utf16_bin DEFAULT NULL,
  `sponsor_comment` text COLLATE utf16_bin,
  `sponsor_approved` enum('Yes','No') COLLATE utf16_bin DEFAULT NULL,
  `sponsor_image_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`sponsor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_sponsor: ~5 rows (approximately)
/*!40000 ALTER TABLE `fashion_sponsor` DISABLE KEYS */;
INSERT INTO `fashion_sponsor` (`sponsor_id`, `sponsor_name`, `sponsor_contact_firstname`, `sponsor_contact_lastname`, `sponsor_email`, `sponsor_phone`, `sponsor_address`, `sponsor_comment`, `sponsor_approved`, `sponsor_image_salt`) VALUES
	(1, 'The guy', 'contac', 'name', 's.davis1902@gmail.com', '1235', 'asd', 'This is the comment', 'Yes', 'd5d'),
	(2, 'Second Sponsor', 'Frank', 'Basket', 'frank@basket.com', '55555', 'asdf', 'thiuasogdo;', 'Yes', 'd8d'),
	(3, 'buujurec', 'ekdjwvho', 'ugwbwomv', 'sample@email.tst', '555-666-0606', '3137 Laguna Street', '1', NULL, NULL),
	(4, 'favpjrdk', 'kkjdgobm', 'yhlldxen', 'sample@email.tst', '555-666-0606', '3137 Laguna Street', '1', NULL, NULL),
	(5, 'mdwmbset', 'hiubasnd', 'vhmhqkmu', 'sample@email.tst', '555-666-0606', '3137 Laguna Street', '1', NULL, NULL);
/*!40000 ALTER TABLE `fashion_sponsor` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_volunteer
DROP TABLE IF EXISTS `fashion_volunteer`;
CREATE TABLE IF NOT EXISTS `fashion_volunteer` (
  `volunteer_id` int(11) NOT NULL AUTO_INCREMENT,
  `volunteer_firstname` char(50) COLLATE utf16_bin DEFAULT NULL,
  `volunteer_lastname` char(50) COLLATE utf16_bin DEFAULT NULL,
  `volunteer_address` char(150) COLLATE utf16_bin DEFAULT NULL,
  `volunteer_email` char(100) COLLATE utf16_bin DEFAULT NULL,
  `volunteer_phone` char(15) COLLATE utf16_bin DEFAULT NULL,
  `volunteer_date_birth` int(10) DEFAULT NULL,
  `volunteer_french` char(3) COLLATE utf16_bin DEFAULT 'No',
  `volunteer_english` char(3) COLLATE utf16_bin DEFAULT 'No',
  `volunteer_education` char(3) COLLATE utf16_bin DEFAULT 'No',
  `volunteer_gender` char(6) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`volunteer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_volunteer: ~1 rows (approximately)
/*!40000 ALTER TABLE `fashion_volunteer` DISABLE KEYS */;
INSERT INTO `fashion_volunteer` (`volunteer_id`, `volunteer_firstname`, `volunteer_lastname`, `volunteer_address`, `volunteer_email`, `volunteer_phone`, `volunteer_date_birth`, `volunteer_french`, `volunteer_english`, `volunteer_education`, `volunteer_gender`) VALUES
	(1, 'Scott', 'Davis', '100 Tideland Drive', 's_davis1902@hotmail.com', '5165007759', 831787200, 'No', 'Yes', 'Yes', 'Male');
/*!40000 ALTER TABLE `fashion_volunteer` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.fashion_volunteer_resend
DROP TABLE IF EXISTS `fashion_volunteer_resend`;
CREATE TABLE IF NOT EXISTS `fashion_volunteer_resend` (
  `resend_id` int(11) NOT NULL AUTO_INCREMENT,
  `resend_volunteer_id` int(11) NOT NULL DEFAULT '0',
  `resend_date_sent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`resend_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.fashion_volunteer_resend: ~1 rows (approximately)
/*!40000 ALTER TABLE `fashion_volunteer_resend` DISABLE KEYS */;
INSERT INTO `fashion_volunteer_resend` (`resend_id`, `resend_volunteer_id`, `resend_date_sent`) VALUES
	(1, 1, 1399858726);
/*!40000 ALTER TABLE `fashion_volunteer_resend` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.module
DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` char(80) COLLATE utf16_bin DEFAULT NULL,
  `module_location` char(150) COLLATE utf16_bin DEFAULT NULL,
  `module_status` enum('Active','Inactive') COLLATE utf16_bin NOT NULL DEFAULT 'Active',
  `module_login` enum('Yes','No') COLLATE utf16_bin NOT NULL DEFAULT 'No',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.module: ~1 rows (approximately)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`module_id`, `module_name`, `module_location`, `module_status`, `module_login`) VALUES
	(1, 'Fashion', 'fashion', 'Active', 'Yes');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.site_folder
DROP TABLE IF EXISTS `site_folder`;
CREATE TABLE IF NOT EXISTS `site_folder` (
  `folder_id` int(11) NOT NULL AUTO_INCREMENT,
  `folder_order` int(11) NOT NULL DEFAULT '0',
  `folder_hidden` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `folder_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `folder_name` char(50) NOT NULL DEFAULT '',
  `folder_url` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`folder_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table sd_Fashion.site_folder: 6 rows
/*!40000 ALTER TABLE `site_folder` DISABLE KEYS */;
INSERT INTO `site_folder` (`folder_id`, `folder_order`, `folder_hidden`, `folder_status`, `folder_name`, `folder_url`) VALUES
	(1, 15, 'No', 'Active', 'Events', 'events'),
	(8, 1, 'No', 'Active', 'About', 'about'),
	(4, 15, 'No', 'Active', 'Sponsor', 'sponsor'),
	(5, 20, 'Yes', 'Active', 'Home', 'home'),
	(6, 19, 'No', 'Active', 'Models', 'models'),
	(7, 18, 'No', 'Active', 'Designers', 'designers');
/*!40000 ALTER TABLE `site_folder` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.site_footer
DROP TABLE IF EXISTS `site_footer`;
CREATE TABLE IF NOT EXISTS `site_footer` (
  `footer_id` int(10) NOT NULL AUTO_INCREMENT,
  `footer_name` char(50) COLLATE utf16_bin DEFAULT NULL,
  `footer_url` char(100) COLLATE utf16_bin DEFAULT NULL,
  `footer_page_id` int(10) DEFAULT NULL,
  `footer_order` int(10) DEFAULT NULL,
  PRIMARY KEY (`footer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.site_footer: ~2 rows (approximately)
/*!40000 ALTER TABLE `site_footer` DISABLE KEYS */;
INSERT INTO `site_footer` (`footer_id`, `footer_name`, `footer_url`, `footer_page_id`, `footer_order`) VALUES
	(1, 'About', '', 7, 19),
	(2, 'Become a Judge', '', 13, 9);
/*!40000 ALTER TABLE `site_footer` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.site_page
DROP TABLE IF EXISTS `site_page`;
CREATE TABLE IF NOT EXISTS `site_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_folder_id` int(11) NOT NULL DEFAULT '0',
  `page_order` int(11) NOT NULL DEFAULT '0',
  `page_hidden` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `page_module_id` int(11) NOT NULL DEFAULT '0',
  `page_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `page_name` char(50) NOT NULL DEFAULT '',
  `page_url` char(50) NOT NULL DEFAULT '',
  `page_content` text NOT NULL,
  `page_redirect` char(100) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table sd_Fashion.site_page: 9 rows
/*!40000 ALTER TABLE `site_page` DISABLE KEYS */;
INSERT INTO `site_page` (`page_id`, `page_folder_id`, `page_order`, `page_hidden`, `page_module_id`, `page_status`, `page_name`, `page_url`, `page_content`, `page_redirect`) VALUES
	(6, 4, 1, 'No', 0, 'Active', 'Sponsor', 'become', '', 'home/index/sponsor'),
	(5, 5, 20, 'No', 1, 'Active', 'Home', 'index', '', ''),
	(8, 1, 1, 'No', 0, 'Active', 'Events', 'index', '<p>This is the new content</p>\r\n', 'home/index/event'),
	(7, 8, 1, 'No', 0, 'Active', 'About', 'index', '<p><strong>About AFAMS:</strong></p>\r\n\r\n<p>African Fashion Models Search &ldquo;AFAMS&rdquo; is a capacity building event organized to promote African culture and groom upcoming individuals in the fashion and modelling industries. The event which is holding in Africa, Canada and Barbados is proposed to select distinctive upcoming African fashion models which will include male and female to compete for a handsome reward and a contract deal in Canada. A thorough selection will be primarily conducted in Africa and Canada (representing the rest of the world) to provide ten (10) representatives each which will make up an overall twenty (20) contestants that will slug it out in Barbados at the grand finale. All registrations must be done online through the AFAMS official website and with the designated banks. Interested contestants are advised to choose locations of proximity within African and Canada during registrations for convenient participation at the auditions in their respective regions. Also, contestants will be required to obtain their garments from AFAMS recommended designers within their regions; the names and locations of AFAMS recommended designers will be released on this website before the auditions. This event is aimed at raising funds for ACTLAP medical mission and peace initiative in Africa.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>The Objectives:</strong></p>\r\n\r\n<ul>\r\n	<li>To raise funds for ACTLAP medical mission and peace initiative in Africa.</li>\r\n	<li>To promote African fashion and cultural essence.</li>\r\n	<li>To expose upcoming models and fashion designers to the fashion and modelling industries.&nbsp;</li>\r\n	<li>To eliminate mediocrity in the African world of fashion and modelling.&nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', ''),
	(9, 6, 20, 'No', 0, 'Active', 'Models', 'index', '', 'home/index/participant/redirect'),
	(10, 7, 20, 'No', 0, 'Active', 'Designers', 'index', '', 'home/index/designer/redirect'),
	(13, 5, 10, 'Yes', 0, 'Active', 'Become a Judge', 'judge', '', 'home/index/judge'),
	(11, 5, 1, 'Yes', 0, 'Active', 'Terms', 'terms', '<p>Coming soon...</p>\r\n', ''),
	(12, 5, 1, 'Yes', 0, 'Active', 'Privacy', 'privacy', '<p>Coming soon...</p>\r\n', '');
/*!40000 ALTER TABLE `site_page` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_username` char(60) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `user_password` char(64) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `user_salt` char(3) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `user_firstname` char(100) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `user_lastname` char(100) COLLATE utf16_bin NOT NULL DEFAULT '0',
  `user_status` enum('Active','Inactive') COLLATE utf16_bin NOT NULL,
  `user_email` char(100) COLLATE utf16_bin NOT NULL,
  `user_admin` enum('Yes','No') COLLATE utf16_bin NOT NULL DEFAULT 'No',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `user_username`, `user_password`, `user_salt`, `user_firstname`, `user_lastname`, `user_status`, `user_email`, `user_admin`) VALUES
	(1, 'admin', '507b4f22413a0fef2aa49156e0c3bd7ad87d0804ba3ce15c567222a91a4f53a3', 'f3f', 'Scott', 'Davis', 'Active', 's_davis1902@hotmail.com', 'Yes'),
	(2, 'paul', '7141c6f29e4cb5a9064f1938d4d975e14cfbc3de06c147bc6de3ad1de515f1b6', 'b63', 'Paul', 'Paul', 'Active', 'paul@paul.com', 'No'),
	(3, 'tester', '7a4229565235aa6270ffbfa6aa2d5d9d12bd369833020be427e5736b100b0487', '935', 'tester', 'mcgoo', 'Active', 'tester@mcgoo.com', 'Yes');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table sd_Fashion.user_forgot
DROP TABLE IF EXISTS `user_forgot`;
CREATE TABLE IF NOT EXISTS `user_forgot` (
  `forgot_id` int(10) NOT NULL AUTO_INCREMENT,
  `forgot_code` char(64) COLLATE utf16_bin DEFAULT NULL,
  `forgot_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `forgot_date_expire` int(11) DEFAULT NULL,
  `forgot_user_id` int(11) DEFAULT NULL,
  `forgot_link_salt` char(3) COLLATE utf16_bin DEFAULT NULL,
  `forgot_link_code` char(64) COLLATE utf16_bin DEFAULT NULL,
  PRIMARY KEY (`forgot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Dumping data for table sd_Fashion.user_forgot: ~19 rows (approximately)
/*!40000 ALTER TABLE `user_forgot` DISABLE KEYS */;
INSERT INTO `user_forgot` (`forgot_id`, `forgot_code`, `forgot_salt`, `forgot_date_expire`, `forgot_user_id`, `forgot_link_salt`, `forgot_link_code`) VALUES
	(1, '4ee584499abe9c6e348ba7e26f000d1fd6d4588ead5927ee922ee5e60b953f4b', '05a', 1390338380, 2, '055', 'd22b931328f10da732ec0fa7ff3d3d306a5d6d710fb9650aaf3010a84b5cd12c'),
	(2, '3bb28deff0507fa95fb1248b6ec9c925d2be185bbf64d10175c8eee26e2699b3', '1c1', 1390338448, 2, 'ed9', 'f7bd74d61b0f9437102af7b4b558ca1e0697b20005d0ce913688fd8cd8a8065c'),
	(3, 'ac45ee4ff0860923b7064f0637886c65761dc18c4ac085bbc68ca13f2909dfe3', '6be', 1390339005, 2, 'f5b', '41894d9778a277c2b1f40cd69562a338b312a715ad7be7cacc50675dc772fb08'),
	(4, '96312b588910269c01dcb954b1818c8f91a2cbd1ab285c54926d063ae7ccf99a', 'aa5', 0, 2, '786', '698efab72826a43751fee0e083820f05a0753a819cd004f4353ee1e19b51e2f4'),
	(5, 'a180f792902f6841d442a9b352702c5ced661c29f6f47c5ced19af8117785a26', '338', 0, 2, '59c', '1deaa7af6cfad8c6d8e8d76f8421026d00d39f3ad704222c8bf178c93ed81ca1'),
	(6, 'cfe07ccb5c42dcab3cd1493dedc618d0ed88af0744f12c4f7394769400af0448', 'a8e', 0, 2, 'f86', '32380c5eb0aa7017c380757c6c0aad3a05f162b39c1e2f43457d6c1f9228eaa7'),
	(7, 'f59fbf5b2fb983c5593068a56ba5037e67d8509295333ca8046fe51cacc06e98', '597', 0, 2, 'eb1', 'cf343f7ab4064d584f43aa7e8854c9adfaafbf9f93423dde854dcf044e5d1336'),
	(8, 'ac2b92cfb4e8a676ace8f86d7a8994b8fc9d3f48b365e70f8f4e73a3ec579dab', 'b84', 0, 2, 'f86', 'b6334db8e9f2f65edf5fe23549afd37daedd578241bf46649fcede6b021d0565'),
	(9, 'd7534e694bee861924fbceeba647ffcf186bdcd5348ef56182453d3358666b61', '4a3', 0, 2, '8f7', '492a49b701c9d90b9a66bf9b8848238ffd45bc256085c791de88133df84c3697'),
	(10, '02f648e4108eba6fe346d94e3373c405009925ffe4159957cbc2bcaca72768c0', 'e98', 1390361271, 1, '664', 'fba9bc8646d48c6594364ce7c86f9901b521038149806912f71ff4a88f184838'),
	(11, 'ff6324157a1a9e17a13c1e3e6a098ed896ffe5bc2554cf2e70f60d843d3d72a1', '0f8', 1390361293, 1, '5fb', '8cc7f5ca0b8696965547352f7ce8e81d91c36dbb4d3ba48e72debe59a35dbc76'),
	(12, '2a09ad3c0cf822723f6e96904b4c621863a9fadc870ef2cfd488369566913616', 'f9e', 1390361398, 1, '4ca', '7b59cf7be26f428cf2755d2ec423e4fb51cc00b27c5aadad11259228afa83b9d'),
	(13, 'edede3b0afc280c382416400ca2aca1fa418016b99c663990678b0243b8fb84c', 'eb2', 1390361445, 1, 'fff', '22d6e49c11fcad60a5e62d87bf94ba7ace427617a423ece4e9f887d7bad35c5b'),
	(14, 'cf8adf90153aace86addc46ce5e8e9f063277a54b72c06bf02202ca13913a242', '964', 1390361809, 1, 'a82', '0ccf5f8b5a2dc5c77c86945cae1f6f44e7eee07ebfd19f36542212272b0d851e'),
	(15, 'ce1f81a3e6b834c85f4e2d327a293cb47723767bd4de14e244d82a0479c653de', 'b81', 1390362144, 1, 'ac7', '122936c47d54b6f1a1d25d0409df4a29014f243333e3bb8e11e4e8a7871ec3d1'),
	(16, 'b07451c95a12d75f94365b51dd0835daf95e39ac083188b4efa640692d7398b8', 'eb3', 1390515158, 0, 'fa2', '77d8e31780d4362422268a0520d5875eb0883eb53d90ef4068c6c9b9df3752fe'),
	(17, 'df994559412f6632014f4c891b6eb5fc8b8d22b0fa65a2ba0a6c41d8a745105b', '515', 1390515269, 1, '93c', '577e9c7318e475a5afd33de615a3d9720f72d310819366ce1e1ddecbeede11a5'),
	(18, 'cf43587cf5a69cc1bb98f744e56f7f01cda505efd67c9eb31728ce56d254ae28', '99b', 1390863264, 1, '598', '20b0bc29aadea7f43766be2422864f7d8574f215b6daa08b9b6bed98af7e5908'),
	(19, 'd59b8d4492bce056fd1f13d8bb97244f2c457d42b55ccfbf21bccf84645de919', '87d', 1391738354, 1, 'cf5', '8c4e63141621d4f3657964ba784b22ea9c07c1251fcfeeda24a68cefb3885cef');
/*!40000 ALTER TABLE `user_forgot` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
