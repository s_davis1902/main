<?php
spl_autoload_register(function ($class) {
	global $CFG;
	if( is_file($CFG->adminserverroot.'/_includes/classes/' . $class . '.php') ){
		include $CFG->adminserverroot.'/_includes/classes/' . $class . '.php';
	}
});

// creating database object
$DB = new MyPDO( $CFG->dbname, $CFG->dbuser, $CFG->dbpassword );
$DB->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

$PAGE = 0;
$USER = 0;

if( isset( $admin ) && $admin ){
	$PAGE = new AdminPage();
	$login = new Login();
	if( !$USER = $login->isLoggedIn() ){
		$PAGE->redirect('login/login.php');
	}
	if( isset( $useradmin ) && $useradmin && $USER->user_admin == 'No' ){ //non admin user trying to access an admin page, redirect them
		$PAGE->redirect('');	
	}
}elseif( isset( $login ) && $login ){
	$PAGE = new AdminPage();
}else{
	
	$theme = 'bare'; // make this dynamic in the future
	$themefile = $CFG->adminserverroot.'/themes/'.$theme.'/'.ucfirst($theme).'Theme.php';
	if( file_exists( $themefile ) ){
		include_once( $themefile );
		$class = ucfirst($theme).'Theme';
		$PAGE = new $class();
	}else{
		$PAGE = new SitePage();
	}
}
?>
