<?php
class AdminPage extends Page {
	
	protected $pagetype = 'admin';
	private $pageurl = '';
	private $pagename = '';
	private $pagesmallname = '';
	private $pagebreadcrumb = '';
	
	private $listIcon = '<i class="fa fa-list"></i>';
	private $editIcon = '<i class="fa fa-pencil-square-o"></i>';
	private $folderIcon = '<i class="fa fa-folder"></i>';
	private $pageIcon = '<i class="fa fa-file-o"></i>';
	private $deleteIcon = '<i class="fa fa-ban"></i>';
	private $dashboardIcon = '<i class="fa fa-dashboard"></i>';
	
	public function setPageName( $pagename ){
		$this->pagename = $pagename;	
	}
	
	public function getPageName(){
		return $this->pagename;	
	}
	
	public function setPageSmallName( $pagesmallname ){
		$this->pagesmallname = $pagesmallname;
	}
	
	public function getPageSmallName(){
		return $this->pagesmallname;
	}
	
	public function setPageBreadCrumb( $breadcrumb ){
		$this->pagebreadcrumb = $breadcrumb;
	}
	
	public function getPageBreadCrumb(){
		if( $this->pagebreadcrumb ){
			return "<ol class=\"breadcrumb\">$this->pagebreadcrumb</ol>";
		}
		return '';
	}
	
	public function setType( $type ){
		$this->pagetype = $type;	
	}
	
	public function getType(){
		return $this->pagetype;	
	}
	
	public function setUrl( $url ){
		$this->pageurl = $url;	
	}
	
	public function setWarning( $warning ){
		$_SESSION['pagewarning'] = $warning;	
	}
	
	private function getWarning(){
		if( isset( $_SESSION['pagewarning'] ) ) return $_SESSION['pagewarning'];
		return 0;	
	}
	
	public function outputWarning(){
		if( $warning = $this->getWarning() ){
			$this->setWarning( '' );
			return "<div class=\"alert alert-info alert-dismissable\">
              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
              $warning
            </div>";
		}
		return '';
	}
	
	public function adminMenuTop(){
		global $DB, $CFG, $USER;
		
		echo "<ul class=\"nav navbar-nav navbar-right navbar-user\">\n";
		
		$adminfilter = '';
		if( $USER->admin == 'No' ){
			$adminfilter = " AND admin = 'No'";	
		}
		
		$folders = $DB->query ( "SELECT * FROM admin_menu WHERE parent=0 AND location='Top' $adminfilter ORDER BY order DESC" );
		
		if ($folders->rowCount ()) {
			while ( $folder = $folders->fetch () ) {
				$pages = $DB->query ( "SELECT * FROM admin_menu WHERE parent=? $adminfilter ORDER BY order DESC", $folder->id );
		
				if ($pages->rowCount ()) {
					if ($pages->rowCount () == 1) {
						// no sub items, just one menu item
						$page = $pages->fetch();
						echo "<li><a href='" . $CFG->adminwebroot . '/' . $page->url . "'>";
						echo ( strlen( $folder->faicon ) ) ? '<i class="fa fa-'.$folder->faicon.'"></i> ' : '';
						echo $folder->name . "</a></li>\n";
						continue;
					}
					echo "<li class=\"dropdown\"><a href='#' class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
					echo ( strlen( $folder->faicon ) ) ? '<i class="fa fa-'.$folder->faicon.'"></i> ' : '';
					echo $fref->name . "</a>\n";
					echo "<ul class=\"dropdown-menu\">\n";
					while ( $page = $pages->fetch () ) {
						echo "<li><a href='" . $CFG->adminwebroot . '/' . $page->url . "'>";
						echo ( strlen( $page->faicon ) ) ? '<i class="fa fa-'.$page->faicon.'"></i> ' : '';
						echo $page->name . "</a></li>\n";
					}
					echo "</ul>\n";
					echo "</li>\n";
				}
			}
		}
		echo '<li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user">
				</i> '.$USER->firstname.' '.$USER->lastname.' <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="'.$CFG->adminwebroot.'/core/user_form.php?id='.$USER->id.'"><i class="fa fa-user"></i> Profile</a></li>
                <li class="divider"></li>
                <li><a href="'.$CFG->adminwebroot.'/login/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>';
		echo "</ul>\n";
	}
	
	public function adminMenu(){
		global $DB, $CFG, $USER;
		
		$adminfilter = '';
		if( $USER->admin == 'No' ){
			$adminfilter = " AND admin = 'No'";
		}
	
		$folders = $DB->query ( "SELECT * FROM admin_menu WHERE parent=0 AND location='Side' $adminfilter ORDER BY order DESC" );
	
		if ($folders->rowCount ()) {
			echo "<ul class=\"nav navbar-nav side-nav\">\n";
			while ( $folder = $folders->fetch () ) {
				$pages = $DB->query ( "SELECT * FROM admin_menu WHERE parent=? $adminfilter ORDER BY order DESC", $folder->id );
	
				if ($pages->rowCount ()) {
					if ($pages->rowCount () == 1) {
						// no sub items, just one menu item
						$page = $pages->fetch ();
						echo "<li><a href='" . $CFG->adminwebroot . '/' . $page->url . "'>";
						echo ( strlen( $folder->faicon ) ) ? '<i class="fa fa-'.$folder->faicon.'"></i> ' : '';
						echo $folder->name . "</a></li>\n";
						continue;
					}
					echo "<li class=\"dropdown\"><a href='#' class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
					echo ( strlen( $folder->faicon ) ) ? '<i class="fa fa-'.$folder->faicon.'"></i> ' : '';
					echo "$folder->name<b class=\"caret\"></b></a>\n";
					echo "<ul class=\"dropdown-menu\">\n";
					while ( $page = $pages->fetch () ) {
						echo "<li><a href='" . $CFG->adminwebroot . '/' . $page->url . "'>";
						echo ( strlen( $page->faicon ) ) ? '<i class="fa fa-'.$page->faicon.'"></i> ' : '';
						echo $page->name . "</a></li>\n";
					}
					echo "</ul>\n";
					echo "</li>\n";
				}
			}
			echo "</ul>\n";
		}
	}
	
	public function getListIcon(){
		return $this->listIcon;	
	}
	
	public function getEditIcon(){
		return $this->editIcon;
	}
	
	public function getFolderIcon(){
		return $this->folderIcon;
	}
	
	public function getPageIcon(){
		return $this->pageIcon;
	}
	
	public function getSubPageIcon(){
		return "&nbsp&nbsp&nbsp".$this->pageIcon;
	}
	
	public function getDeleteIcon(){
		return $this->deleteIcon;
	}
	
	public function getDashboardIcon(){
		return $this->dashboardIcon;
	}
}
?>
