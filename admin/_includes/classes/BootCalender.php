<?php
class BootCalender {
	
	private $events = array();
	private $eventsUrl = 0;
	
	public function render(){
		global $CFG;
		
		echo '<div class="container">';
		
		echo '
		<div class="page-header">
			<div class="pull-right form-inline">
				<div class="btn-group">
					<button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
					<button class="btn" data-calendar-nav="today">Today</button>
					<button class="btn btn-primary" data-calendar-nav="next">Next >></button>
				</div>
				<div class="btn-group">
					<button class="btn btn-warning" data-calendar-view="year">Year</button>
					<button class="btn btn-warning active" data-calendar-view="month">Month</button>
					<button class="btn btn-warning" data-calendar-view="week">Week</button>
					<button class="btn btn-warning" data-calendar-view="day">Day</button>
				</div>
			</div>
				<h3></h3>
		</div>';
		
		$events_source = '';
		if( $this->eventsUrl ){
			$events_source = "'$this->eventsUrl'";
		}else{
			$events = $this->getJson();
			$events_source = "function(){return $events;}";
		}
		
		echo '<script type="text/javascript" src="'.$CFG->adminwebroot.'/_includes/js/underscore.min.js"></script>';
		echo '<script type="text/javascript" src="'.$CFG->adminwebroot.'/_includes/js/calendar.js"></script>';
		
		echo '<div id="calendar"></div>';
		echo "<script type=\"text/javascript\">
			var calendar = \$('#calendar').calendar({events_source: $events_source});
		</script>";
	}
	
	public function setEventsUrl( $url ){
		$this->eventsUrl = $url;	
	}
	
	public function getJson(){
		return json_encode($this->events);	
	}
	
	public function addEvent($id, $title, $url, $start, $end, $class='event-info'){
		global $CFG;
		
		$this->events[] = array(
				'id'    => $id,
				'title' => $title,
				'url'   => $url,
				'start' => $start.'000',
				'end'   => $end.'000',
				'class' => $class
		);
	}
}
?>