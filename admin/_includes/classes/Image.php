<?php
class Image {
	protected static function serveImage( $url, $type = 'png' ){
		global $CFG;
		
		if( $type == 'jpg' ){
			header('Content-type: image/jpeg;');
		}elseif( $type == 'gif' ){
			header('Content-Type: image/gif');
		}else{
			header('Content-Type: image/png');
		}
		
		echo file_get_contents("$CFG->serverroot/$url");
	}
	
	protected static function serveNoImage(){
		
	}
}
?>