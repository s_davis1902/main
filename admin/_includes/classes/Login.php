<?php
class Login {
	
	private $error = '';
	
	function __construct( $loginTable = 'user', $tablePrefix = 'user_' ){
		$this->loginTable = $loginTable;
		$this->tablePrefix = $tablePrefix;
	}
	
	public function getError(){
		return $this->error;	
	}
	
	public function setError( $error ){
		$this->error = $error;	
	}
	
	public function authenticateUser( $username, $password ){
		global $DB;
		
		error_log( "$username $password" );
		
		$table = $this->loginTable;
		$idField = $this->tablePrefix.'id';
		$usernameField = $this->tablePrefix.'username';
		$saltField = $this->tablePrefix.'salt';
		$passwordField = $this->tablePrefix.'password';
		
		if( !$user = $DB->getRecord("* FROM $table WHERE $usernameField=?", $username) ){
			$this->setError( 'Incorrect Username' );
			return 0;
		}
		
		$confirmfield = $this->tablePrefix.'confirmed';
		if( isset( $user->$confirmfield ) && $user->$confirmfield != 'Yes' ){
			$this->setError( 'Account has not been confirmed' );
			return 0;
		}
		
		if( !$this->checkPassword( $user->$saltField, $user->$passwordField, $password) ){
			$this->setError( 'Incorrect Password' );
			return 0;
		}
		
// 		$hash = hash('sha256', $user->$saltField . hash('sha256', $password) );
// 		if( $hash != $user->$passwordField ){
// 			$this->setError( 'Incorrect Password' );
// 			return 0;
// 		}
		
		$user->$passwordField = 'egads';
		
		$this->validateUser( $user->$idField );
		
		return $user;
	}
	
	public function checkPassword( $salt, $hash, $password ){
		$inputhash = hash('sha256', $salt . hash('sha256', $password) );
		if( $inputhash != $hash ){
			$this->setError( 'Incorrect Password' );
			return 0;
		}
		
		return 1;
	}
	
	private function validateUser( $id ){
	    session_regenerate_id(); //this is a security measure
	    $_SESSION[$this->tablePrefix.'valid'] = 1;
	    $_SESSION[$this->tablePrefix.'id'] = $id;
	}
	
	public function isLoggedIn(){
		global $DB;
	    if(isset($_SESSION[$this->tablePrefix.'valid']) && $_SESSION[$this->tablePrefix.'valid']){
	    	$table = $this->loginTable;
	    	$idField = $this->tablePrefix.'id';
	    	$passwordField = $this->tablePrefix.'password';
	    	
	    	if( $user = $DB->getRecord("* FROM $table WHERE $idField=?", $_SESSION[$this->tablePrefix.'id']) ){
	    		$user->$passwordField = 'egads';
	    		return $user;
	    	}
	    }
	    return 0;
	}
	
	public function logout(){
		$_SESSION = array(); //destroy all of the session variables
		session_destroy();
	}
	
	public static function hashPassword( $password, $salt ){
		return hash('sha256', $salt . hash('sha256', $password));
	}
	
	public static function generateSalt( $length = 3 ){
		$string = md5(uniqid(rand(), true));
		return substr($string, 0, $length);
	}
	
	public function setPassword( $id, $password, $updateonusername = 0 ){
		global $DB;
		
		$updatefield = $this->tablePrefix.'id';
		if( $updateonusername ){
			$updatefield = $this->tablePrefix.'username';	
		}
		
		$salt = $this->generateSalt();
		$password = $this->hashPassword( $password, $salt );
		$DB->update($this->loginTable, array(
				$this->tablePrefix.'password' => $password,
				$this->tablePrefix.'salt'     => $salt
		), array(
				$updatefield                  => $id
		));
	}
	
	/******************************
	 * ****************************
	 * ****************************
	 * Forgot Password Section ****/
	
	private function setForgotCode( $user_id ){
		global $DB;
		
		$expire = time() + ( 60 * 5 ); // 5 minutes
		$salt = $this->generateSalt();
		$code = $this->generateSalt( 8 );
		$hashedcode = $this->hashPassword( $code, $salt );
		
		$linksalt = $this->generateSalt();
		$linkcode = $this->generateSalt( 8 );
		$linkhashedcode = $this->hashPassword( $linkcode, $linksalt );
		
		$DB->insert( "{$this->loginTable}_forgot", array(
			"forgot_{$this->tablePrefix}id" => $user_id,
			"forgot_code"                    => $hashedcode,
			"forgot_date_expire"             => $expire,
			"forgot_salt"                    => $salt,
			"forgot_link_salt"               => $linksalt,
			"forgot_link_code"               => $linkhashedcode
		));
		
		return array( $code, $linkcode );
	}
	
	private function emailForgotCode( $user_id, $code, $type ){
		global $DB, $CFG;
		
		error_log( "HERE" );
		
		$user = $DB->getRecord("* FROM $this->loginTable WHERE {$this->tablePrefix}id = ?", $user_id );
		
		if( !$user ) return false;
		
		$emailfield = $this->tablePrefix.'email';
		
		$mail = new PHPMailer();
		
		$mail->isSMTP();
// 		$mail->SMTPDebug = 2;
// 		$mail->Debugoutput = 'html';
		$mail->Host = $DB->getConfigField( 'core', 'smtp_host' );
		$mail->Port = $DB->getConfigField( 'core', 'smtp_port' );
		$mail->SMTPSecure = $DB->getConfigField( 'core', 'smtp_security' );
		$mail->SMTPAuth = true;
		$mail->Username = $DB->getConfigField( 'core', 'smtp_username' );
		$mail->Password = $DB->getConfigField( 'core', 'smtp_password' );
		
		$mail->setFrom($DB->getConfigField( 'core', 'forgot_email' ), $DB->getConfigField( 'core', 'forgot_name' ));
		$mail->addReplyTo($DB->getConfigField( 'core', 'forgot_email' ), $DB->getConfigField( 'core', 'forgot_name' ));
		$mail->addAddress($user->$emailfield);
		
		$message = "";
		if( $type == 'signup' ){
			$mail->Subject = 'Signup Confirmation';
			
			$message = "An account has been registered at $CFG->webroot using your email address.  If this was not you, please ignore this email\n\n";
			$message.= "If this was you, please copy the code below and enter it in the form on the website in order to confirm your account.\n\n";
			$message.= "$code";
		}elseif( $type == 'username' ){
			$mail->Subject = 'Forgot Username';
				
			$message = "A forgot username request was made with your email address at $CFG->webroot.  If this was not you, please ignore this email\n\n";
			$message.= "If this was you, please copy the code below and enter it in the form on the website in order to reset your username.\n\n";
			$message.= "$code";
		}else{
			$mail->Subject = 'Forgot Password';
			
			$message = "A forgot password request was made with your email address at $CFG->webroot.  If this was not you, please ignore this email\n\n";
			$message.= "If this was you, please copy the code below and enter it in the form on the website in order to reset your password.\n\n";
			$message.= "$code";
		}
		
		$html = "<html><body>".nl2br($message)."</body></html>";
		
		$mail->msgHTML($html, dirname(__FILE__));
		$mail->AltBody = $message;
		
		if (!$mail->send()) {
			error_log( "HERE - FAIL - $mail->ErrorInfo" );
			return false;
		}
		error_log( "HERE - SUCCESS" );
		return true;
	}
	
	public function checkForgotCode( $username, $code, $link = 0 ){
		global $DB;
		
		if( $link ){
			$saltfield = 'forgot_link_salt';
			$codefield = 'forgot_link_code';
		}else{
			$saltfield = 'forgot_salt';
			$codefield = 'forgot_code';	
		}
		
		$user = $DB->getRecord( "* FROM {$this->loginTable} WHERE {$this->tablePrefix}username = ? ORDER BY {$this->tablePrefix}id DESC", $username );
		$useridfield = "{$this->tablePrefix}id";
		$userid = $user->$useridfield;
		
		$query = "f.* FROM {$this->loginTable}_forgot f, $this->loginTable u 
			WHERE f.forgot_{$this->tablePrefix}id = u.{$this->tablePrefix}id
			AND u.{$this->tablePrefix}id = ?
			AND f.forgot_date_expire > ?";
		$ref = $DB->getRecord($query, $userid, time());
		
		if( $ref && $this->hashPassword( $code, $ref->$saltfield ) == $ref->$codefield ){
			if( !$link ){ // email code, can only be attempted once, so now we will expire it
				$DB->update( "{$this->loginTable}_forgot", 
					array( 'forgot_date_expire' => 0 ), 
					array( 'forgot_id' => $ref->forgot_id )
				);
			}
			return true;
		}
		return false;
	}
	
	public function checkForgotLinkCode( $username, $code ){
		return $this->checkForgotCode( $username, $code, 1 );	
	}
	
	public function checkForgotEmail( $email, $type = 'password' ){
		global $DB;
		
		$user = $DB->getRecord("* from $this->loginTable WHERE {$this->tablePrefix}email = ? ORDER BY {$this->tablePrefix}id DESC", $email);
		
		if( $user ){
			$idfield = "{$this->tablePrefix}id";
			list( $code, $linkcode ) = $this->setForgotCode( $user->$idfield );
			$this->emailForgotCode( $user->$idfield, $code, $type );
			return $linkcode;
		}else{
			list( $code, $linkcode ) = $this->setForgotCode( 0 );
			return $linkcode;	
		}
	}
	
	public function sendUsername( $username ){
		global $DB;

		$user = $DB->getRecord("* from $this->loginTable WHERE {$this->tablePrefix}username = ? ORDER BY {$this->tablePrefix}id DESC", $username);
		
		if( !$user ) return false;
		
		$emailfield = $this->tablePrefix.'email';
		
		$mail = new PHPMailer();
		
		$mail->isSMTP();
// 		$mail->SMTPDebug = 2;
// 		$mail->Debugoutput = 'html';
		$mail->Host = $DB->getConfigField( 'core', 'smtp_host' );
		$mail->Port = $DB->getConfigField( 'core', 'smtp_port' );
		$mail->SMTPSecure = $DB->getConfigField( 'core', 'smtp_security' );
		$mail->SMTPAuth = true;
		$mail->Username = $DB->getConfigField( 'core', 'smtp_username' );
		$mail->Password = $DB->getConfigField( 'core', 'smtp_password' );
		
		$mail->setFrom($DB->getConfigField( 'core', 'forgot_email' ), $DB->getConfigField( 'core', 'forgot_name' ));
		$mail->addReplyTo($DB->getConfigField( 'core', 'forgot_email' ), $DB->getConfigField( 'core', 'forgot_name' ));
		$mail->addAddress($user->$emailfield);
		$mail->Subject = 'Your Username';
		
		$message = "Here is your username for loging into $CFG->webroot.\n\n";
		$message.= "$username\n\n";
		
		$html = "<html><body>".nl2br($message)."</body></html>";
		
		$mail->msgHTML($html, dirname(__FILE__));
		$mail->AltBody = $message;
		
		if (!$mail->send()) {
			return false;
		}
		return true;
	}
	
	/******************************
	 * ****************************
	 * ****************************
	 * Brute Force protection  ****/
	//http://www.bryanrite.com/preventing-brute-force-attacks-on-your-web-login/
	//http://www.codedevelopr.com/articles/throttle-user-login-attempts-in-php/
	/*
	 * will track all failed attempts, create a function for saving that data to the database
	 * another function will query the database to get number of failed attemps in past n minutes
	 * login page, when loading form will check this in order to see if captcha is needed or not
	 * login processing page will check this to see if it needs to delay before processing?? or is there a better way to do the delay??
	 */
}
?>