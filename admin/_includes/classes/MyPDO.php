<?php
class MyPDO extends PDO {
		
	public function __construct( $dbname, $dbuser, $dbpass, $dbhost = 'localhost', $dbport = '3306', $options = null) {
		parent::__construct ( 'mysql:host=' . $dbhost . ';port=' . $dbport . ';dbname=' . $dbname, $dbuser, $dbpass, $options );
	}
	
	public function query($query) { // secured query with prepare and execute
		$args = func_get_args ();
		array_shift ( $args ); // first element is not an argument but the query itself, should removed
		
		$response = parent::prepare ( $query );
		$response->execute ( $args );
		return $response;
	}
	
	public function get($query) { // secured query with prepare and execute
		$args = func_get_args ();
		array_shift ( $args ); // first element is not an argument but the query itself, should removed
		
		$query = trim ( $query );
		
		$field = substr ( $query, 0, strpos ( $query, ' ' ) );
		$query = "SELECT " . $query;
		
		$response = parent::prepare ( $query );
		$response->execute ( $args );
		
		$ref = $response->fetch ();
		
		return (isset ( $ref->$field )) ? $ref->$field : false;
	}
	
	public function getRecord($query) { // secured query with prepare and execute
		$args = func_get_args ();
		array_shift ( $args ); // first element is not an argument but the query itself, should removed
	
		$query = trim ( $query );
	
		$query = "SELECT " . $query;
	
		$response = parent::prepare ( $query );
		$response->execute ( $args );
	
		$ref = $response->fetch ();
	
		return $ref;
	}
	
	public function insert($table, $values) { // takes a table name and array of fields and values and creates a safe insert
		$fields = '';
		$val = '';
		$args = array ();
		foreach ( $values as $key => $value ) {
			$fields .= "" . $key . ",";
			$val .= "?,";
			array_push ( $args, $value );
		}
		$fields = substr ( $fields, 0, - 1 ); // remove the last comma
		$val = substr ( $val, 0, - 1 ); // remove the last comma
		$query = "INSERT INTO " . $table . " (" . $fields . ") VALUES (" . $val . ")";
		
		$response = parent::prepare ( $query );
		$response->execute ( $args );
		
		return $response;
	}
	
	public function update($table, $values, $conditions) {
		$update = '';
		$cond = '';
		$args = array ();
		
		foreach ( $values as $key => $value ) {
			$update .= "" . $key . "=?,";
			array_push ( $args, $value );
		}
		if (strlen ( $update ) == 0)
			return false;
		$update = substr ( $update, 0, - 1 ); // remove last comma
		
		foreach ( $conditions as $key => $value ) {
			$cond .= "" . $key . "=? AND ";
			array_push ( $args, $value );
		}
		if (strlen ( $cond ) == 0)
			return false;
		$cond = substr ( $cond, 0, - 4 ); // remove last AND
		
		$query = "UPDATE " . $table . " SET " . $update . " WHERE " . $cond;
		
		$response = parent::prepare ( $query );
		$response->execute ( $args );
		
		return $response;
	}
	
	public function getOptions( $table, $key, $value, $where = '' ){
		$args = func_get_args ();
		array_shift ( $args );
		array_shift ( $args );
		array_shift ( $args );
		array_shift ( $args );
		
		if( $where ) $where = " WHERE $where";
		
		$response = parent::prepare ( "SELECT $key, $value FROM $table $where" );
		$response->execute ( $args );
		
		$options = array();
		if ($response->rowCount ()) {
			while ( $ref = $response->fetch () ) {
				$options[$ref->$key] = $ref->$value;
			}
		}
		return $options;
	}
	
	public function saveFromPost( $table, $keyname, $keyvalue, $fields ){		
		$values = array();
		
		foreach( $fields as $field ){
			if( isset( $_POST[$field] ) ){
				$values[$field] = $_POST[$field];	
			}
		}
		
		if( $keyvalue ){
			$this->update( $table, $values, array( $keyname => $keyvalue ) );
		}else{
			$this->insert( $table, $values );
		}
		return $this->lastInsertId();
	}
	
	public function saveConfigField( $area, $field, $value ){
		global $DB;
	
		$id = $DB->get('config_id FROM config WHERE config_area = ? AND config_field = ?', $area, $field );
	
		if( $id ){
			$DB->update( 'config', array( 'config_value' => $value ), array( 'config_id' => $id ) );
		}else{
			$DB->insert( 'config', array(
					'config_area'  => $area,
					'config_field' => $field,
					'config_value' => $value
			) );
		}
	
		$this->Config[$area][$field] = $value;
	}
	
	public function getConfigField( $area, $field ){
		global $DB;
		if( !isset( $this->Config[$area][$field] ) ){
			$value = $DB->get('config_value FROM config WHERE config_area = ? AND config_field = ?', $area, $field );
			if( $value ) $this->Config[$area][$field] = $value;
			else return false;
		}
	
		return $this->Config[$area][$field];
	}
}
