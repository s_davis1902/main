<?php
class Element_Status extends Element_Select {
	public function __construct($label, $name, array $properties = null) {
		$options = array(
			"Active"   => "Active",
			"Inactive" => "Inactive"
		);

		if(!is_array($properties))
			$properties = array("inline" => 1);
		elseif(!array_key_exists("inline", $properties))
			$properties["inline"] = 1;
		
		parent::__construct($label, $name, $options, $properties);
    }
}
