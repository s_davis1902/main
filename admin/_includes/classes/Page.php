<?php
class Page {
	public function redirect( $url ){
		global $CFG;
		
		if( $this->pagetype == 'admin' ){
			$redirect = $CFG->adminwebroot.'/'.$url;
		}else{
			$redirect = $CFG->webroot.'/'.$url;
		}
		
		if( isset( $CFG->debug ) && $CFG->debug ){
			error_log( "redirect: $redirect" );	
		}
		
		header("Location: $redirect");
		exit(0);
	}
}
?>