<?php
class SitePage extends Page {
	protected $pagetype = 'site';
	private $pageTitle = '';
	private $moduleId = 0;
	private $module = 0;
	private $pageContent = '';
	private $params=0;
	public $theme='basic';
	
	private function getPageTitle(){
		return $this->pageTitle;	
	}
	
	public function setPageTitle( $title ){
		$this->pageTitle = $title;	
	}
	
	public function processHeader(){
		global $CFG;
		if( !$this->moduleId ) return ''; // no module so no special header stuff
		include_once($CFG->adminserverroot.'/'.$this->module->module_location.'/classes/Module'.ucfirst($this->module->module_location).'.php');
		$modulename = 'Module'.ucfirst($this->module->module_location);
		$module = new $modulename();
		$module->processHeader( $this->params );
	}
	
	private function getContent(){
		global $CFG;
		if( $this->moduleId ){
			include_once($CFG->adminserverroot.'/'.$this->module->module_location.'/classes/Module'.ucfirst($this->module->module_location).'.php');
			$modulename = 'Module'.ucfirst($this->module->module_location);
			$module = new $modulename();
			$content = $module->getContent( $this->params );
			return $content ? $content : $this->go404();
		}else{
			return $this->pageContent;
		}
	}

    public function getRequestUrl(){
        global $CFG;

        return str_replace( $CFG->webroot.'/', '', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    }
	
	public function processUrl( $request ){
		global $DB, $CFG;
		
		$page = 0;
		$params = explode('/', $request);
		
		if( isset( $params[1] ) && $params[1] ){ //page and folder
			$sql = '* FROM site_folder f, site_page p WHERE f.folder_id = p.page_folder_id AND f.folder_url=? AND p.page_url = ? ORDER BY page_order DESC LIMIT 1';
			$page = $DB->getRecord($sql, $params[0], $params[1]);
			if( $page && $page->page_redirect ){
				$this->redirect($page->page_redirect);	
			}
		}elseif( isset( $params[0] ) && $params[0] ){ // just folder
			if( $params[0] == '404' ){
				$this->pageContent = $this->get404Content();
				$this->setPageTitle( $this->get404Title() );
				return 1;
			}else{
				$sql = '* FROM site_folder f, site_page p WHERE f.folder_id = p.page_folder_id AND f.folder_url=? ORDER BY p.page_order DESC LIMIT 1';
				$page = $DB->getRecord($sql, $params[0]);
				$this->redirect($page->folder_url.'/'.$page->page_url);	
			}
		}else{ // homepage
			$page = $DB->getRecord('* FROM site_page p, site_folder f WHERE p.page_folder_id = f.folder_id AND p.page_id = ? LIMIT 1', $DB->getConfigField( 'core', 'homepage' ) );
			$this->redirect($page->folder_url.'/'.$page->page_url);	
		}
		
		if( !$page ){
			$this->go404();
			return 1;	
		}
		
		$this->pageTitle = $page->page_name;
		
		if( $page->page_module_id ){
			$module =$DB->getRecord('* FROM module WHERE module_id = ?', $page->page_module_id);
			
			if(!file_exists($CFG->adminserverroot.'/'.$module->module_location.'/classes/Module'.ucfirst($module->module_location).'.php')){
				$this->go404();
				return 1;
			}
			
			$this->moduleId = $module->module_id;
			$this->module = $module;
		}else{
			if( count( $params ) > 2 ){
				$this->go404();
				return 1;	
			}
			$this->pageContent = $page->page_content;
		}
		
		$this->params = $params;
		return 1;
	}
	
	public function outputPage(){
		global $CFG;
		$themeserverurl = $CFG->adminserverroot.'/themes/'.$this->theme;
		$themeweburl = $CFG->adminwebroot.'/themes/'.$this->theme;
		
		include($themeserverurl.'/default.php');
	}
	
	private function get404Title(){
		return '404 Not Found';
	}
	
	private function get404Content(){
		return 'We could not find this page';	
	}
	
	private function go404(){
		$this->redirect('404');	
	}
	
 	public function siteMenu(){
 		global $DB, $CFG;
	
 		echo "<ul class=\"nav navbar-nav\">\n";
	
 		$query = "SELECT * FROM site_folder 
 				WHERE folder_status='Active' AND folder_hidden='No' 
 				ORDER BY folder_order DESC";
 		$fhan = $DB->query ( $query );
	
 		if ($fhan->rowCount ()) {
 			while ( $fref = $fhan->fetch () ) {
				
 				$query2 = "SELECT * FROM site_page 
 						WHERE page_folder_id=? AND page_status='Active' AND page_hidden='No' 
 						ORDER BY page_order DESC";
 				$phan = $DB->query ( $query2, $fref->folder_id );
		
 				if ($phan->rowCount ()) {
 					if ($phan->rowCount () == 1) {
 						// no sub items, just one menu item
 						$pref = $phan->fetch ();
 						echo "<li><a href='" . $CFG->webroot . "/".$fref->folder_url."/" . $pref->page_url . "'>";
 						echo $pref->page_name . "</a></li>\n";
 						continue;
 					}
 					echo "<li class=\"dropdown\"><a href='#' class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
 					echo $fref->folder_name . "</a>\n";
 					echo "<ul class=\"dropdown-menu\">\n";
 					while ( $pref = $phan->fetch () ) {
 						echo "<li><a href='" . $CFG->webroot . "/".$fref->folder_url."/" . $pref->page_url . "'>";
 						echo $pref->page_name . "</a></li>\n";
 					}
 					echo "</ul>\n";
 					echo "</li>\n";
 				}
 			}
 		}
 		echo "</ul>\n";
		
 		// if logged in
 		if( isset( $CFG->front_username ) && isset( $CFG->front_profileurl ) && isset( $CFG->front_logouturl ) ){
 			echo "<ul class=\"nav navbar-nav navbar-right navbar-user\">\n";
 			echo '<li class="dropdown user-dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user">
 				</i> '.$CFG->front_username.' <b class="caret"></b></a>
               <ul class="dropdown-menu">
                 <li><a href="'.$CFG->front_profileurl.'"><i class="fa fa-user"></i> Profile</a></li>';
             if( isset( $CFG->front_dashboardurl ) ){
 				echo '<li><a href="'.$CFG->front_dashboardurl.'"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
             }
             echo '<li class="divider"></li>
                 <li><a href="'.$CFG->front_logouturl.'"><i class="fa fa-power-off"></i> Log Out</a></li>
               </ul>
             </li>';
 			echo "</ul>\n";
 		}elseif( isset( $CFG->front_loginname ) && isset( $CFG->front_loginurl ) ){
 			echo "<ul class=\"nav navbar-nav navbar-right navbar-user\">\n";
 			echo '<li>
               <a href="'.$CFG->front_loginurl.'"><i class="fa fa-user">
 				</i> '.$CFG->front_loginname.'</a>
             </li>';
 			echo "</ul>\n";
 		}
 	}
	
	public function checkModuleLogin(){
		global $CFG, $DB;
		
		$module = $DB->getRecord( '* FROM module WHERE module_login="Yes"' );
		
		if( $module ){
			include_once($CFG->adminserverroot.'/'.$module->module_location.'/classes/Module'.ucfirst($module->module_location).'.php');
			$modulename = 'Module'.ucfirst($module->module_location);
			$mdl = new $modulename();
			$user = $mdl->checkLogin( $this->params, $module );
			if( $user ){
				return $user;	
			}
		}
		return false;
	}
}
?>
