<?php
// deletion confirmation for list pages
echo "<script src='$CFG->adminwebroot/_includes/js/confirm-bootstrap.js'></script>";
$message = isset($label) ? "Are you sure you want to delete this $label?" : "Are you sure you want to delete this Item?";
echo "<script type=\"text/javascript\">
$('a[href*=\"?disolve\"]').click(function (e) {
     e.preventDefault()
 })

 $('a[href*=\"?disolve\"]').confirmModal({
    confirmTitle     : 'Please Confirm',
    confirmMessage   : '$message',
    confirmOk        : 'Yes',
    confirmCancel    : 'No'
});
</script>";
?>
</div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->
  </body>
</html>
