<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - SB Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $CFG->adminwebroot; ?>/_includes/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo $CFG->adminwebroot; ?>/_includes/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $CFG->adminwebroot; ?>/_includes/font-awesome/css/font-awesome.min.css">
    <link href="<?php echo $CFG->adminwebroot; ?>/_includes/css/calendar.css" rel="stylesheet">
    
    <script src="//code.jquery.com/jquery-latest.min.js"></script>
    <script src="<?php echo $CFG->adminwebroot; ?>/_includes/js/bootstrap.js"></script>
    <!-- Page Specific Plugins -->
    <script src="<?php echo $CFG->adminwebroot; ?>/_includes/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?php echo $CFG->adminwebroot; ?>/_includes/js/tablesorter/tables.js"></script>
    
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $CFG->adminwebroot; ?>"><?php echo $PAGE->getDashboardIcon();?> Dashboard</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          	<?php 
          	if( !isset( $nomenu ) || !$nomenu ){
	          	$PAGE->adminMenu(); 
	          	$PAGE->adminMenuTop();
	        } 
          	?>
          
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">
      <?php
      echo "<div class=\"row\">
          <div class=\"col-lg-12\">
            <h1>".$PAGE->getPageName()." <small>".$PAGE->getPageSmallName()."</small></h1>
            ".$PAGE->getPageBreadCrumb()."
            ".$PAGE->outputWarning()."
          </div>
        </div><!-- /.row -->";
      ?>