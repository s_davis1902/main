<?php

error_reporting(E_ALL);

// Start Session
session_start();

$CFG = new stdClass;

$CFG->webroot = 'http://sd.main.com';
$CFG->serverroot = '/var/www/html/sd/Main';
$CFG->adminwebroot = $CFG->webroot.'/admin';
$CFG->adminserverroot = $CFG->serverroot.'/admin';

$CFG->dbname = 'SdMain';
$CFG->dbuser = 'root';
$CFG->dbpassword = 'root';

$CFG->debug = 1;

$CFG->passwordrulesdescription = "Password must be 6 to 20 characters, contain at least one letter, one number and one uppercase letter.";
$CFG->passwordrulesarray = array(
		"/^.{6,20}$/" => "Error: The %element% field must be between 6 and 20 characters",
		"/[A-Z]/"     => "Error: The %element% field must contain at least one uppercase character",
		"/\w/"        => "Error: The %element% field must contain at least one letter and one number",
		"/\d/"        => "Error: The %element% field must contain at least one letter and one number"
);

include_once('_includes/autoload.php');
?>
