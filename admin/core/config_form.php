<?php
$admin=1;
$useradmin=1;
include '../config.php';

$PAGE->setURL('core/config_form.php');
$label = 'Config';

if( isset( $_POST['admin-config-form-submit'] ) ){
	$formname = "admin-config-form";
	if( Form::isValid($formname) ){
		
		$fields = array( 'forgot_email', 'smtp_host', 'smtp_port', 'smtp_security', 'smtp_username', 'smtp_password', 'forgot_name', 'homepage' );
		foreach( $fields as $field ){
			if( isset( $_POST['core_'.$field] ) && $_POST['core_'.$field] ){
				$DB->saveConfigField( 'core', $field, $_POST['core_'.$field] );
			}
		}
		
		$PAGE->redirect('core/config_form.php');
	}else{
		$PAGE->redirect('core/config_form.php');
	}
}

$actTypeLabel = 'Update';
$breadcrumb = "<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = "admin-config-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

$form->addElement(new Element_Hidden("admin-config-form-submit", 1));

/****************************************
 ***************************************/
$options = $DB->getOptions('site_page', 'page_id' , 'page_name', '');
$core_homepage = $DB->getConfigField( 'core', 'homepage' );
$form->addElement(new Element_Select("Homepage:", "core_homepage", $options, array(
		'value'      => isset( $core_homepage ) ? $core_homepage : '',
		'class'      => "form-control"
)));
$form->addElement(new Element_HTML('<br />'));
/****************************************
 ***************************************/
$core_forgot_email = $DB->getConfigField( 'core', 'forgot_email' );
$form->addElement(new Element_Textbox("Forgot Password Email:", "core_forgot_email", array(
		'value'      => isset( $core_forgot_email ) && $core_forgot_email ? $core_forgot_email : '', 
		'required'   => 1,
		'class'      => "form-control"
)));
$core_forgot_name = $DB->getConfigField( 'core', 'forgot_name' );
$form->addElement(new Element_Textbox("Forgot Password Email Name:", "core_forgot_name", array(
		'value'      => isset( $core_forgot_name ) && $core_forgot_name ? $core_forgot_name : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_HTML('<br />'));
/****************************************
 ***************************************/
$core_smtp_host = $DB->getConfigField( 'core', 'smtp_host' );
$form->addElement(new Element_Textbox("SMTP Host:", "core_smtp_host", array(
		'value'      => isset( $core_smtp_host ) && $core_smtp_host ? $core_smtp_host : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$core_smtp_port = $DB->getConfigField( 'core', 'smtp_port' );
$form->addElement(new Element_Textbox("SMTP Port:", "core_smtp_port", array(
		'value'      => isset( $core_smtp_port ) && $core_smtp_port ? $core_smtp_port : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$core_smtp_security = $DB->getConfigField( 'core', 'smtp_security' );
$form->addElement(new Element_Textbox("SMTP Security:", "core_smtp_security", array(
		'value'      => isset( $core_smtp_security ) && $core_smtp_security ? $core_smtp_security : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$core_smtp_username = $DB->getConfigField( 'core', 'smtp_username' );
$form->addElement(new Element_Textbox("SMTP Username:", "core_smtp_username", array(
		'value'      => isset( $core_smtp_username ) && $core_smtp_username ? $core_smtp_username : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$core_smtp_password = $DB->getConfigField( 'core', 'smtp_password' );
$form->addElement(new Element_Textbox("SMTP Password:", "core_smtp_password", array(
		'value'      => isset( $core_smtp_password ) && $core_smtp_password ? $core_smtp_password : '',
		'required'   => 1,
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/

echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>