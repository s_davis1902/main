<?php
$admin=1;
$useradmin=1;
include '../config.php';

$PAGE->setURL('core/menu_form.php');
$label = 'Menu';

if( isset( $_POST['admin-menu-form-submit'] ) ){
	$formname = isset( $_POST['menu_id'] ) ? "admin-menu-form-".$_POST['menu_id'] : "admin-menu-form";
	if( Form::isValid($formname) ){
		
		$fields = array( 'menu_name', 'menu_url', 'menu_parent', 'menu_order', 'menu_location', 'menu_faicon');
		if( isset( $_POST['menu_id'] ) ){
			$DB->saveFromPost( 'admin_menu', 'menu_id', $_POST['menu_id'], $fields );
			$PAGE->setWarning("$label Updated");
		}else{
			$DB->saveFromPost( 'admin_menu', 'menu_id', 0, $fields );
			$PAGE->setWarning("$label Added");
		}
		
		$PAGE->redirect('core/menu_list.php');
	}else{
		if( isset( $_POST['menu_id'] ) ){
			$PAGE->redirect('core/menu_form.php?id='.$_POST['menu_id']);
		}else{
			$PAGE->redirect('core/menu_form.php');
		}
	}
}

if( isset( $_GET['id'] ) ){
	$menu = $DB->getRecord('* FROM admin_menu WHERE menu_id=?', $_GET['id']);	
}

if( isset( $menu->menu_id ) ){
	$actTypeLabel = 'Update';
}else{
	$actTypeLabel = 'New';
}

$breadcrumb = "<li><a href=\"".strtolower($label)."_list.php\">".$PAGE->getListIcon()." $label List</a></li>
<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = isset( $menu->menu_id ) ? "admin-menu-form-$menu->menu_id" : "admin-menu-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

if( isset( $menu->menu_id ) ){
	$form->addElement(new Element_Hidden("menu_id", $menu->menu_id));
}
$form->addElement(new Element_Hidden("admin-menu-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Name:", "menu_name", array(
		'value'      => isset( $menu->menu_name ) ? $menu->menu_name : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("URL:", "menu_url", array(
		'value'      => isset( $menu->menu_url ) ? $menu->menu_url : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$options = array( 0 => 'None' ) + $DB->getOptions('admin_menu', 'menu_id' , 'menu_name', 'menu_parent=0');
$form->addElement(new Element_Select("Parent:", "menu_parent", $options, array(
		'value'      => isset( $menu->menu_parent ) ? $menu->menu_parent : '',
		'class'      => "form-control"
)));
$options = array(); for($i=1;$i<=20;$i++){ $options[] = $i;}
$form->addElement(new Element_Select("Menu Order:", "menu_order", $options, array(
		'value'      => isset( $menu->menu_order ) ? $menu->menu_order : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$options = array( 'Top' => 'Top', 'Side' => 'Side' );
$form->addElement(new Element_Select("Menu Location:", "menu_location", $options, array(
		'value'      => isset( $menu->menu_location ) ? $menu->menu_location : '',
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("fa Icon:", "menu_faicon", array(
		'value'      => isset( $menu->menu_faicon ) ? $menu->menu_faicon : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>