<?php
$admin=1;
$useradmin=1;
include '../config.php';

$PAGE->setURL('core/menu_list.php');
$label = 'Menu';

if( isset( $_GET['disolve'] ) ){
	$DB->query('DELETE FROM admin_menu WHERE menu_id=?', $_GET['disolve']);
	$PAGE->redirect('core/menu_list.php');
}

$breadcrumb = "<li>".$PAGE->getListIcon()." $label List</li>
<li><a href=\"".strtolower($label)."_form.php\">".$PAGE->getEditIcon()." New $label</a></li>";

$PAGE->setPageName( "$label List" );
//$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$tbl = new HTML_Table('', 'table table-bordered table-hover', 1, array('width' => '100%') );

$tbl->addHeader( 'Side Menu' );

//$tbl->addTSection('thead');
$tbl->addRow();
// arguments: cell content, class, type (default is 'data' for td, pass 'header' for th)
// can include associative array of optional additional attributes
$tbl->addCell('Edit', '', 'header');
$tbl->addCell('Icon', '', 'header');
$tbl->addCell('Name', '', 'header');
$tbl->addCell('URL', '', 'header');
$tbl->addCell('Delete', '', 'header');

$tbl->addTSection('tbody');

$han = $DB->query ( "SELECT * FROM admin_menu WHERE menu_parent = 0 AND menu_location='Side' ORDER BY menu_order DESC" );
if ($han->rowCount ()) {
	while ( $ref = $han->fetch () ) {
		$tbl->addRow();
		$tbl->addCell( "<a href='menu_form.php?id=$ref->menu_id'>".$PAGE->getEditIcon()."</a>" );
		$tbl->addCell( "<i class=\"fa fa-$ref->menu_faicon\"></i>" );
		$tbl->addCell( $PAGE->getFolderIcon()." $ref->menu_name" );
		$tbl->addCell( "<a href='$CFG->adminwebroot/$ref->menu_url'>$ref->menu_url</a>" );
		$tbl->addCell( "<a href='menu_list.php?disolve=$ref->menu_id'>".$PAGE->getDeleteIcon()."</a>" );
		$shan = $DB->query ( "SELECT * FROM admin_menu WHERE menu_parent = ? ORDER BY menu_order DESC", $ref->menu_id );
		if ($shan->rowCount ()) {
			while ( $sref = $shan->fetch () ) {
				$tbl->addRow();
				$tbl->addCell( "<a href='menu_form.php?id=$sref->menu_id'>".$PAGE->getEditIcon()."</a>" );
				$tbl->addCell( "<i class=\"fa fa-$sref->menu_faicon\"></i>" );
				$tbl->addCell( $PAGE->getSubPageIcon()." $sref->menu_name" );
				$tbl->addCell( "<a href='$CFG->adminwebroot/$sref->menu_url'>$sref->menu_url</a>" );
				$tbl->addCell( "<a href='menu_list.php?disolve=$sref->menu_id'>".$PAGE->getDeleteIcon()."</a>" );
			}
		}
	}
}else{
	$tbl->addRow();
	$tbl->addCell('No menu items found.', 'foot', 'data', array('colspan'=>5) );
}

echo '<div class="table-responsive">';
echo $tbl->display();
echo '</div>';

$tbl = new HTML_Table('', 'table table-bordered table-hover', 1, array('width' => '100%') );

$tbl->addHeader( 'Top Menu' );

//$tbl->addTSection('thead');
$tbl->addRow();
// arguments: cell content, class, type (default is 'data' for td, pass 'header' for th)
// can include associative array of optional additional attributes
$tbl->addCell('Edit', '', 'header');
$tbl->addCell('Icon', '', 'header');
$tbl->addCell('Name', '', 'header');
$tbl->addCell('URL', '', 'header');
$tbl->addCell('Delete', '', 'header');

$tbl->addTSection('tbody');

$han = $DB->query ( "SELECT * FROM admin_menu WHERE menu_parent = 0 AND menu_location='Top' ORDER BY menu_order DESC" );
if ($han->rowCount ()) {
	while ( $ref = $han->fetch () ) {
		$tbl->addRow();
		$tbl->addCell( "<a href='menu_form.php?id=$ref->menu_id'>".$PAGE->getEditIcon()."</a>" );
		$tbl->addCell( "<i class=\"fa fa-$ref->menu_faicon\"></i>" );
		$tbl->addCell( $PAGE->getFolderIcon()." $ref->menu_name" );
		$tbl->addCell( "<a href='$CFG->adminwebroot/$ref->menu_url'>$ref->menu_url</a>" );
		$tbl->addCell( "<a href='menu_list.php?disolve=$ref->menu_id'>".$PAGE->getDeleteIcon()."</a>" );
		$shan = $DB->query ( "SELECT * FROM admin_menu WHERE menu_parent = ? ORDER BY menu_order DESC", $ref->menu_id );
		if ($shan->rowCount ()) {
			while ( $sref = $shan->fetch () ) {
				$tbl->addRow();
				$tbl->addCell( "<a href='menu_form.php?id=$sref->menu_id'>".$PAGE->getEditIcon()."</a>" );
				$tbl->addCell( "<i class=\"fa fa-$sref->menu_faicon\"></i>" );
				$tbl->addCell( $PAGE->getSubPageIcon()." $sref->menu_name" );
				$tbl->addCell( "<a href='$CFG->adminwebroot/$sref->menu_url'>$sref->menu_url</a>" );
				$tbl->addCell( "<a href='menu_list.php?disolve=$sref->menu_id'>".$PAGE->getDeleteIcon()."</a>" );
			}
		}
	}
}else{
	$tbl->addRow();
	$tbl->addCell('No menu items found.', 'foot', 'data', array('colspan'=>5) );
}

echo '<div class="table-responsive">';
echo $tbl->display();
echo '</div>';

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>