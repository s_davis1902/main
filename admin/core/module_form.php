<?php
$admin=1;
$useradmin=1;
include '../config.php';

$PAGE->setURL('core/module_form.php');
$label = 'Module';

if( isset( $_POST['admin-module-form-submit'] ) ){
	$formname = isset( $_POST['module_id'] ) ? "admin-module-form-".$_POST['module_id'] : "admin-module-form";
	if( Form::isValid($formname) ){
		
		$fields = array( 'module_name', 'module_location', 'module_status', 'module_login');
		if( isset( $_POST['module_id'] ) ){
			$DB->saveFromPost( 'module', 'module_id', $_POST['module_id'], $fields );
			$PAGE->setWarning("$label Updated");
		}else{
			$DB->saveFromPost( 'module', 'module_id', 0, $fields );
			$PAGE->setWarning("$label Added");
		}
		$PAGE->redirect('core/module_list.php');
	}else{
		if( isset( $_POST['module_id'] ) ){
			$PAGE->redirect('core/module_form.php?id='.$_POST['module_id']);
		}else{
			$PAGE->redirect('core/module_form.php');
		}
	}
}

if( isset( $_GET['id'] ) ){
	$module = $DB->getRecord('* FROM module WHERE module_id=?', $_GET['id']);	
}

if( isset( $module->module_id ) ){
	$actTypeLabel = 'Update';
}else{
	$actTypeLabel = 'New';
}

$breadcrumb = "<li><a href=\"".strtolower($label)."_list.php\">".$PAGE->getListIcon()." $label List</a></li>
<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = isset( $module->module_id ) ? "admin-module-form-$module->module_id" : "admin-module-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

if( isset( $module->module_id ) ){
	$form->addElement(new Element_Hidden("module_id", $module->module_id));
}
$form->addElement(new Element_Hidden("admin-module-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Name:", "module_name", array(
		'value'      => isset( $module->module_name ) ? $module->module_name : '', 
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("Location:", "module_location", array(
		'value'      => isset( $module->module_location ) ? $module->module_location : '',
		'required'   => 1,
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Status("Status:", "module_status", array(
		'value'      => isset( $module->module_status ) ? $module->module_status : '',
		'class'      => "form-control"
)));
$form->addElement(new Element_YesNo("Front end Authentication:", "module_login", array(
		'value'      => isset( $module->module_login ) ? $module->module_login : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/

echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>