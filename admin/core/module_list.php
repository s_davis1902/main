<?php
$admin=1;
$useradmin=1;
include '../config.php';

$PAGE->setURL('core/module_list.php');
$label = 'Module';

if( isset( $_GET['disolve'] ) ){
	$DB->query('DELETE FROM module WHERE module_id=?', $_GET['disolve']);
	$PAGE->redirect('core/module_list.php');
}

$breadcrumb = "<li>".$PAGE->getListIcon()." $label List</li>
<li><a href=\"".strtolower($label)."_form.php\">".$PAGE->getEditIcon()." New $label</a></li>";

$PAGE->setPageName( "$label List" );
//$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$tbl = new HTML_Table('', 'table table-bordered table-hover tablesorter', 1, array('width' => '100%') );

$tbl->addTSection('thead');
$tbl->addRow();
// arguments: cell content, class, type (default is 'data' for td, pass 'header' for th)
// can include associative array of optional additional attributes
$tbl->addCell('Edit', '', 'header');
$tbl->addCell('Name  <i class="fa fa-sort"></i>', '', 'header');
$tbl->addCell('Location  <i class="fa fa-sort"></i>', '', 'header');
$tbl->addCell('Status  <i class="fa fa-sort"></i>', '', 'header');
$tbl->addCell('Delete', '', 'header');

$tbl->addTSection('tbody');

$han = $DB->query ( "SELECT * FROM module ORDER BY module_name ASC" );
if ($han->rowCount ()) {
	while ( $ref = $han->fetch () ) {
		$tbl->addRow();
		$tbl->addCell( "<a href='module_form.php?id=$ref->module_id'>".$PAGE->getEditIcon()."</a>" );
		$tbl->addCell( "$ref->module_name" );
		$tbl->addCell( $ref->module_location );
		$tbl->addCell( $ref->module_status );
		$tbl->addCell( "<a href='module_list.php?disolve=$ref->module_id'>".$PAGE->getDeleteIcon()."</a>" );
	}
}else{
	$tbl->addRow();
	$tbl->addCell('No modules found.', 'foot', 'data', array('colspan'=>5) );
}

echo '<div class="table-responsive">';
echo $tbl->display();
echo '</div>';

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>