<?php
$admin=1;
include '../config.php';

if( $USER->user_admin == 'No' && ( !isset( $_GET['id'] ) || $_GET['id'] != $USER->user_id ) ){
	$PAGE->redirect('');	
}

$PAGE->setURL('core/user_form.php');
$label = 'User';

if( isset( $_POST['admin-user-form-submit'] ) ){
	$formname = isset( $_POST['user_id'] ) ? "admin-user-form-".$_POST['user_id'] : "admin-user-form";
	
	// must manually check if password and confirmpassword match
	$noerrors = true;
	$error_array = array();
	if( !isset( $_POST['user_password'] ) || !isset( $_POST['confirm_password'] ) || $_POST['user_password'] != $_POST['confirm_password'] ){
		$noerrors = false;
		$error_array+= array( "Password and Confirm Password do not match" => 'user_password' );
	}
	
	$id = isset( $_POST['user_id'] ) ? $_POST['user_id'] : 0;
	if( $DB->get('user_id FROM user WHERE user_username = ? AND user_id <> ?', $_POST['user_username'], $id ) ){
		$noerrors = false;
		$error_array+= array( "Username is already taken, please choose another" => 'user_username' );
	}
	
	if( $DB->get('user_id FROM user WHERE user_email = ? AND user_id <> ?', $_POST['user_email'], $id ) ){
		$noerrors = false;
		$error_array+= array( "Email is already taken, please choose another" => 'user_email' );
	}
	
if( !isset( $_POST['check_pw_scr'] ) || $_POST['check_pw_scr'] < 70 ){
		$noerrors = false;
		$error_array+= array( "Your password is not strong enough" => 'user_password' );
	}
	
	if( isset( $_POST['check_pw_errors'] ) && strlen( $_POST['check_pw_errors'] ) > 0 ){
		$noerrors = false;
		$psw_errors = explode( ',', $_POST['check_pw_errors'] );
		foreach( $psw_errors as $psw_error ){
			if( !$psw_error ) continue;
			$error_array+= array( "Password Error: $psw_error" => 'user_password' );
		}
	}
	
	if( Form::isValid($formname, $noerrors) && $noerrors ){	
		$fields=0;
		if( $USER->user_admin == 'Yes' ){
			$fields = array('user_username','user_firstname','user_lastname','user_status','user_email','user_admin');
		}else{
			$fields = array('user_username','user_firstname','user_lastname','user_status','user_email');
		}
		if( isset( $_POST['user_id'] ) ){
			$DB->saveFromPost( 'user', 'user_id', $_POST['user_id'], $fields );
			$PAGE->setWarning("$label Updated");
		}else{
 			$_POST['user_id'] = $DB->saveFromPost( 'user', 'user_id', 0, $fields );
			$PAGE->setWarning("$label Added");
		}
		
		if( strlen( $_POST['user_password'] ) > 0 ){
			$login = new Login();
			$login->setPassword( $_POST['user_id'], $_POST['user_password'] );
		}
		
		$PAGE->redirect('core/user_list.php');
	}else{
		// add any extra errors
		if( $error_array ){
			foreach( $error_array as $error => $field ){
				Form::setError($formname, $error, $field);
			}
		}
		if( isset( $_POST['user_id'] ) ){
			$PAGE->redirect('core/user_form.php?id='.$_POST['user_id']);
		}else{
			$PAGE->redirect('core/user_form.php');
		}
	}
}

if( isset( $_GET['id'] ) ){
	$user = $DB->getRecord('* FROM user WHERE user_id=?', $_GET['id']);	
}

if( isset( $user->user_id ) ){
	$actTypeLabel = 'Update';
}else{
	$actTypeLabel = 'New';
}

$breadcrumb = "<li><a href=\"".strtolower($label)."_list.php\">".$PAGE->getListIcon()." $label List</a></li>
<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = isset( $user->user_id ) ? "admin-user-form-$user->user_id" : "admin-user-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

if( isset( $user->user_id ) ){
	$form->addElement(new Element_Hidden("user_id", $user->user_id));
}
$form->addElement(new Element_Hidden("admin-user-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Username:", "user_username", array(
		'value'      => isset( $user->user_username ) ? $user->user_username : '', 
		'required'   => 1, 
		'validation' => new Validation_AlphaNumeric,
		'class'      => "form-control"
)));

// Password field stuff
$passwordvars = array( 'class' => "form-control" );
if( !isset( $user->user_id ) ) $passwordvars+= array( 'required' => 1 );
$confirmpasswordvars = $passwordvars;
$form->addElement(new Element_Password('Password:', "user_password", $passwordvars));
$form->addElement(new Element_Hidden("check_pw_scr", '0'));
$form->addElement(new Element_Hidden("check_pw_errors", ''));
$form->addElement(new Element_Password('Confirm Password:', "confirm_password", $confirmpasswordvars));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Textbox("FirstName:", "user_firstname", array(
		'value'      => isset( $user->user_firstname ) ? $user->user_firstname : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("LastName:", "user_lastname", array(
		'value'      => isset( $user->user_lastname ) ? $user->user_lastname : '',
		'required'   => 1,
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Status("Status:", "user_status", array(
		'value'      => isset( $user->user_status ) ? $user->user_status : '',
		'class'      => "form-control"
)));
if( $USER->user_admin == 'Yes' ){
	$form->addElement(new Element_YesNo("Admin:", "user_admin", array(
			'value'      => isset( $user->user_admin ) ? $user->user_admin : '',
			'class'      => "form-control"
	)));
}
$form->addElement(new Element_Email("Email:", "user_email", array(
		'value'      => isset( $user->user_email ) ? $user->user_email : '',
		'required'   => 1,
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

echo "<script type='text/javascript' src='$CFG->adminwebroot/_includes/js/pwstrength.js'></script>";
echo "<script type=\"text/javascript\">
        jQuery(document).ready(function () {
            \"use strict\";
            var options = {};
            options.ui = {
                showVerdictsInsideProgressBar: true,
				showErrors: true
            };
			options.common = {
                usernameField: '[name=user_username]'
            };
			options.rules = {
                activated: {
                    wordTwoCharacterClasses: true,
                    wordRepetitions: true
                }
            };
            
            $('[name=user_password]').pwstrength(options);	
	    });
    </script>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>