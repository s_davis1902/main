<?php
$admin=1;
$useradmin=1;
include '../config.php';

$PAGE->setURL('core/user_form.php');
$label = 'User';

if( isset( $_GET['disolve'] ) ){
	$DB->query('DELETE FROM user WHERE user_id=?', $_GET['disolve']);
	$PAGE->redirect('core/user_list.php');
}

$breadcrumb = "<li>".$PAGE->getListIcon()." $label List</li>
<li><a href=\"".strtolower($label)."_form.php\">".$PAGE->getEditIcon()." New $label</a></li>";

$PAGE->setPageName( "$label List" );
//$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$tbl = new HTML_Table('', 'table table-bordered table-hover tablesorter', 1, array('width' => '100%') );

$tbl->addTSection('thead');
$tbl->addRow();
// arguments: cell content, class, type (default is 'data' for td, pass 'header' for th)
// can include associative array of optional additional attributes
$tbl->addCell('Edit', '', 'header');
$tbl->addCell('UserName  <i class="fa fa-sort"></i>', '', 'header');
$tbl->addCell('Name  <i class="fa fa-sort"></i>', '', 'header');
$tbl->addCell('Delete', '', 'header');

$tbl->addTSection('tbody');

$han = $DB->query ( "SELECT * FROM user ORDER BY user_lastname ASC" );
if ($han->rowCount ()) {
	while ( $ref = $han->fetch () ) {
		$tbl->addRow();
		$tbl->addCell( "<a href='user_form.php?id=$ref->user_id'>".$PAGE->getEditIcon()."</a>" );
		$tbl->addCell( "$ref->user_username" );
		$tbl->addCell( "$ref->user_lastname, $ref->user_firstname" );
		$tbl->addCell( "<a href='user_list.php?disolve=$ref->user_id'>".$PAGE->getDeleteIcon()."</a>" );
	}
}else{
	$tbl->addRow();
	$tbl->addCell('No users found.', 'foot', 'data', array('colspan'=>4) );
}

echo '<div class="table-responsive">';
echo $tbl->display();
echo '</div>';

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>