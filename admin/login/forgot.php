<?php
$login = 1;
include '../config.php';

if( isset( $_POST['admin-forgot-form-submit'] ) ){
	if( Form::isValid('admin-forgot-form') ){
		$login = new Login();
		
		$code = $login->checkForgotEmail( $_POST['forgot_email'] );
		
		$PAGE->redirect('login/reset.php?code='.$code);
	}else{
		$PAGE->redirect('login/forgot.php');
	}
}

// the header
$nomenu=1;
include $CFG->adminserverroot.'/_includes/gui/header.php';

$form = new Form('admin-forgot-form');
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

$form->addElement(new Element_Hidden("admin-forgot-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Email("Email:", "forgot_email", array(
		'required'   => 1,
		'class'      => "form-control",
		"longDesc" => "Please enter the email address that is registered with your account."
)));
$form->addElement(new Element_Captcha("Captcha:"));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>