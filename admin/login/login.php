<?php
$login = 1;
include '../config.php';

$PAGE->setURL('core/user_form.php');
$label = 'User';

if( isset( $_POST['admin-login-form-submit'] ) ){
	if( Form::isValid('admin-login-form') ){
		$login = new Login();
		
		if( !$user = $login->authenticateUser( $_POST['user_username'], $_POST['user_password'] ) ){
			$PAGE->redirect('login/login.php?error=1');
		}
		
		$PAGE->redirect('index.php');
	}else{
		$PAGE->redirect('login/login.php');
	}
}

// the header
$nomenu=1;
include $CFG->adminserverroot.'/_includes/gui/header.php';

$form = new Form('admin-login-form');
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

$form->addElement(new Element_Hidden("admin-login-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Username:", "user_username", array(
		'required'   => 1, 
		'validation' => new Validation_AlphaNumeric,
		'class'      => "form-control"
)));
$form->addElement(new Element_Password("Password:", "user_password", array(
		'required'   => 1,
		'class'      => "form-control"
)));

//$form->addElement(new Element_Captcha("Captcha:"));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_HTML('<a href="forgot.php">Forgot Password?</a><br />'));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>