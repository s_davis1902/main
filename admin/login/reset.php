<?php
$login = 1;
include '../config.php';

if( isset( $_POST['admin-reset-form-submit'] ) ){
	
	$formname = 'admin-reset-form';
	
	$passwordconfirm = 1;
	if( !isset( $_POST['forgot_password'] ) || !isset( $_POST['confirm_password'] ) || $_POST['forgot_password'] != $_POST['confirm_password'] ){
		$passwordconfirm = 0;
		Form::isValid($formname, false); // call this here, so if there are any other validation errors, they get added to error message
		Form::setError($formname, "Password and Confirm Password do not match", 'forgot_password');
	}
	
	if( $passwordconfirm && Form::isValid($formname) ){
		$login = new Login();
		
		if( $login->checkForgotLinkCode( $_POST['forgot_username'], $_POST['code'] ) && $login->checkForgotCode( $_POST['forgot_username'], $_POST['forgot_code'] ) ){
			$login->setPassword( $_POST['forgot_username'], $_POST['forgot_password'], 1 );
		}
		
		$PAGE->redirect('login/login.php');
	}else{
		$PAGE->redirect('login/reset.php?code='.$_POST['code']);
	}
}

if( !isset( $_GET['code' ] ) ){
	$PAGE->redirect('login/forgot.php');
}

// the header
$nomenu=1;
include $CFG->adminserverroot.'/_includes/gui/header.php';

$form = new Form('admin-reset-form');
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

$form->addElement(new Element_Hidden("admin-reset-form-submit", 1));
$form->addElement(new Element_Hidden("code", $_GET['code']));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Code:", "forgot_code", array(
		'required'   => 1,
		'class'      => "form-control",
		"longDesc"   => "If the email address you entered was found in our database, you should receive an email containing a code.  Please enter it here."
)));
$form->addElement(new Element_Textbox("Username:", "forgot_username", array(
		'required'   => 1,
		'class'      => "form-control"
)));

// Password field stuff
$passwordvars = array( 'class' => "form-control", 'required' => 1 );
$confirmpasswordvars = $passwordvars;
if( isset( $CFG->passwordrulesdescription ) ) $passwordvars+= array( "shortDesc" => $CFG->passwordrulesdescription );
if( isset( $CFG->passwordrulesarray ) ){
	$validationarray = array();
	foreach( $CFG->passwordrulesarray as $key => $value ){
		array_push( $validationarray, new Validation_RegExp($key, $value) );
	}
	$passwordvars+= array( "validation" => $validationarray );
}
$form->addElement(new Element_Password('Password:', "forgot_password", $passwordvars));
$form->addElement(new Element_Password('Confirm Password:', "confirm_password", $confirmpasswordvars));
$form->addElement(new Element_Captcha("Captcha:"));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>