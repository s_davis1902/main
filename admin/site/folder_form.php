<?php
$admin=1;
include '../config.php';

$PAGE->setURL('site/folder_form.php');
$label = 'Folder';

if( isset( $_POST['admin-folder-form-submit'] ) ){
	$formname = isset( $_POST['folder_id'] ) ? "admin-folder-form-".$_POST['folder_id'] : "admin-folder-form";
	if( Form::isValid($formname) ){
		
		$fields = array( 'folder_name', 'folder_url', 'folder_status', 'folder_hidden', 'folder_order');
		if( isset( $_POST['folder_id'] ) ){
			$DB->saveFromPost( 'site_folder', 'folder_id', $_POST['folder_id'], $fields );
			$PAGE->setWarning("$label Updated");
		}else{
			$DB->saveFromPost( 'site_folder', 'folder_id', 0, $fields );
			$PAGE->setWarning("$label Added");
		}
		
		$PAGE->redirect('site/site_view.php');
	}else{
		if( isset( $_POST['folder_id'] ) ){
			$PAGE->redirect('site/folder_form.php?id='.$_POST['folder_id']);
		}else{
			$PAGE->redirect('site/folder_form.php');
		}
	}
}

if( isset( $_GET['id'] ) ){
	$folder = $DB->getRecord('* FROM site_folder WHERE folder_id=?', $_GET['id']);	
}

if( isset( $folder->folder_id ) ){
	$actTypeLabel = 'Update';
}else{
	$actTypeLabel = 'New';
}

$breadcrumb = "<li><a href=\"site_view.php\">".$PAGE->getListIcon()." Site View</a></li>
<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = isset( $folder->folder_id ) ? "admin-folder-form-$folder->folder_id" : "admin-folder-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

if( isset( $folder->folder_id ) ){
	$form->addElement(new Element_Hidden("folder_id", $folder->folder_id));
}
$form->addElement(new Element_Hidden("admin-folder-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Name:", "folder_name", array(
		'value'      => isset( $folder->folder_name ) ? $folder->folder_name : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("URL:", "folder_url", array(
		'value'      => isset( $folder->folder_url ) ? $folder->folder_url : '',
		'required'   => 1,
		'validation' => new Validation_AlphaNumeric,
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$options = array(); for($i=1;$i<=20;$i++){ $options[] = $i;}
$form->addElement(new Element_Select("Order:", "folder_order", $options, array(
		'value'      => isset( $folder->folder_order ) ? $folder->folder_order : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Status("Status:", "folder_status", array(
		'value'      => isset( $folder->folder_status ) ? $folder->folder_status : '',
		'class'      => "form-control"
)));
$form->addElement(new Element_YesNo("Hidden:", "folder_hidden", array(
		'value'      => isset( $folder->folder_hidden ) ? $folder->folder_hidden : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>