<?php
$admin=1;
include '../config.php';

$PAGE->setURL('site/footer_form.php');
$label = 'Footer';

if( isset( $_POST['admin-footer-form-submit'] ) ){
	$formname = isset( $_POST['footer_id'] ) ? "admin-footer-form-".$_POST['footer_id'] : "admin-footer-form";
	if( Form::isValid($formname) ){
		
		$fields = array( 'footer_name', 'footer_url', 'folder_status', 'footer_page_id', 'footer_order');
		if( isset( $_POST['footer_id'] ) ){
			$DB->saveFromPost( 'site_footer', 'footer_id', $_POST['footer_id'], $fields );
			$PAGE->setWarning("$label Updated");
		}else{
			$DB->saveFromPost( 'site_footer', 'fooder_id', 0, $fields );
			$PAGE->setWarning("$label Added");
		}
		
		$PAGE->redirect('site/site_view.php');
	}else{
		if( isset( $_POST['footer_id'] ) ){
			$PAGE->redirect('site/footer_form.php?id='.$_POST['footer_id']);
		}else{
			$PAGE->redirect('site/footer_form.php');
		}
	}
}

if( isset( $_GET['id'] ) ){
	$footer = $DB->getRecord('* FROM site_footer WHERE footer_id=?', $_GET['id']);	
}

if( isset( $footer->footer_id ) ){
	$actTypeLabel = 'Update';
}else{
	$actTypeLabel = 'New';
}

$breadcrumb = "<li><a href=\"site_view.php\">".$PAGE->getListIcon()." Site View</a></li>
<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = isset( $footer->footer_id ) ? "admin-footer-form-$footer->footer_id" : "admin-footer-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

if( isset( $footer->footer_id ) ){
	$form->addElement(new Element_Hidden("footer_id", $footer->footer_id));
}
$form->addElement(new Element_Hidden("admin-footer-form-submit", 1));

/****************************************
 ***************************************/
$form->addElement(new Element_Textbox("Name:", "footer_name", array(
		'value'      => isset( $footer->footer_name ) ? $footer->footer_name : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_Url("URL:", "footer_url", array(
		'value'      => isset( $footer->footer_url ) ? $footer->footer_url : '',
		'class'      => "form-control",
		"shortDesc"  => "If filled, will override Site Page optoin"
)));
$options = array( 0 => 'None' ) + $DB->getOptions('site_page', 'page_id' , 'page_name', '');
$form->addElement(new Element_Select("Site Page:", "footer_page_id", $options, array(
		'value'      => isset( $footer->footer_page_id ) ? $footer->footer_page_id : '',
		'class'      => "form-control",
		"shortDesc"  => "Will only be used if URL is empty"
)));
/****************************************
 ***************************************/
$options = array(); for($i=1;$i<=20;$i++){ $options[] = $i;}
$form->addElement(new Element_Select("Order:", "footer_order", $options, array(
		'value'      => isset( $footer->footer_order ) ? $footer->footer_order : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>