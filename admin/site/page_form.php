<?php
$admin=1;
include '../config.php';

$PAGE->setURL('site/page_form.php');
$label = 'Page';

if( isset( $_POST['admin-page-form-submit'] ) ){
	$formname = isset( $_POST['page_id'] ) ? "admin-page-form-".$_POST['page_id'] : "admin-page-form";
	if( Form::isValid($formname) ){
		
		$fields = array('page_folder_id', 'page_order', 'page_hidden', 'page_module_id', 'page_status', 'page_name', 'page_url', 'page_content', 'page_redirect');
		if( isset( $_POST['page_id'] ) ){
			$DB->saveFromPost( 'site_page', 'page_id', $_POST['page_id'], $fields );
			$PAGE->setWarning("$label Updated");
		}else{
			$DB->saveFromPost( 'site_page', 'page_id', 0, $fields );
			$PAGE->setWarning("$label Added");
		}
		
		$PAGE->redirect('site/site_view.php');
	}else{
		if( isset( $_POST['page_id'] ) ){
			$PAGE->redirect('site/page_form.php?id='.$_POST['page_id']);
		}else{
			$PAGE->redirect('site/page_form.php');
		}
	}
}

if( isset( $_GET['id'] ) ){
	$page = $DB->getRecord('* FROM site_page WHERE page_id=?', $_GET['id']);	
}

if( isset( $page->page_id ) ){
	$actTypeLabel = 'Update';
}else{
	$actTypeLabel = 'New';
}

$breadcrumb = "<li><a href=\"site_view.php\">".$PAGE->getListIcon()." Site View</a></li>
<li>".$PAGE->getEditIcon()." $actTypeLabel $label</li>";

$PAGE->setPageName( "$label Form" );
$PAGE->setPageSmallName( "Enter $label information" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$formname = isset( $page->page_id ) ? "admin-page-form-$page->page_id" : "admin-page-form";

$form = new Form($formname);
$form->configure(array(
		"prevent" => array("bootstrap", "jQuery")
));

if( isset( $page->page_id ) ){
	$form->addElement(new Element_Hidden("page_id", $page->page_id));
}
$form->addElement(new Element_Hidden("admin-page-form-submit", 1));

/****************************************
 ***************************************/
$options = $DB->getOptions('site_folder', 'folder_id' , 'folder_name');
$form->addElement(new Element_Select("Folder:", "page_folder_id", $options, array(
		'value'      => isset( $page->page_folder_id ) ? $page->page_folder_id : '',
		'class'      => "form-control"
)));
$options = array(); for($i=1;$i<=20;$i++){ $options[] = $i;}
$form->addElement(new Element_Select("Page Order:", "page_order", $options, array(
		'value'      => isset( $page->page_order ) ? $page->page_order : '',
		'class'      => "form-control"
)));
$form->addElement(new Element_YesNo("Hidden:", "page_hidden", array(
		'value'      => isset( $page->page_hidden ) ? $page->page_hidden : '',
		'class'      => "form-control"
)));
$options = array( 'None' => 'None' ) + $DB->getOptions('module', 'module_id' , 'module_name', 'module_status="Active"');
$form->addElement(new Element_Select("Module:", "page_module_id", $options, array(
		'value'      => isset( $page->page_module_id ) ? $page->page_module_id : '',
		'class'      => "form-control"
)));
$form->addElement(new Element_Status("Status:", "page_status", array(
		'value'      => isset( $page->page_status ) ? $page->page_status : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Textbox("Name:", "page_name", array(
		'value'      => isset( $page->page_name ) ? $page->page_name : '',
		'required'   => 1,
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("URL:", "page_url", array(
		'value'      => isset( $page->page_url ) ? $page->page_url : '',
		'required'   => 1,
		'validation' => new Validation_AlphaNumeric,
		'class'      => "form-control"
)));
$form->addElement(new Element_Textbox("Redirect:", "page_redirect", array(
		'value'      => isset( $page->page_redirect ) ? $page->page_redirect : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_CKEditor("Content:", "page_content", array(
		'value'      => isset( $page->page_content ) ? $page->page_content : '',
		'class'      => "form-control"
)));
/****************************************
 ***************************************/
$form->addElement(new Element_HTML('<br />'));
$form->addElement(new Element_Button);
/****************************************
 ***************************************/
echo "<div class=\"row\">
          <div class=\"col-lg-8\">";
$form->render();
echo "</div></div>";

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>