<?php
$admin=1;
include '../config.php';

$PAGE->setURL('site/site_view.php');
$label = 'Site View';

if( isset( $_GET['disolve_folder'] ) ){
	$DB->query('DELETE FROM site_folder WHERE folder_id=?', $_GET['disolve_folder']);
	$DB->query('DELETE FROM site_page WHERE page_folder_id=?', $_GET['disolve_folder']);
	$PAGE->redirect('site/site_view.php');
}

if( isset( $_GET['disolve_page'] ) ){
	$DB->query('DELETE FROM site_page WHERE page_id=?', $_GET['disolve_page']);
	$PAGE->redirect('site/site_view.php');
}

if( isset( $_GET['disolve_footer'] ) ){
	$DB->query('DELETE FROM site_footer WHERE footer_id=?', $_GET['disolve_footer']);
	$PAGE->redirect('site/site_view.php');
}

$breadcrumb = "<li>".$PAGE->getListIcon()." $label</li>
<li><a href=\"folder_form.php\">".$PAGE->getEditIcon()." New Folder</a></li>
<li><a href=\"page_form.php\">".$PAGE->getEditIcon()." New Page</a></li>
		<li><a href=\"footer_form.php\">".$PAGE->getEditIcon()." New Footer</a></li>";

$PAGE->setPageName( "$label" );
$PAGE->setPageSmallName( "View Site Layout" );
$PAGE->setPageBreadCrumb( $breadcrumb );

// the header
include $CFG->adminserverroot.'/_includes/gui/header.php';

$tbl = new HTML_Table('', 'table table-bordered table-hover', 1, array('width' => '100%') );

//$tbl->addTSection('thead');
$tbl->addRow();
// arguments: cell content, class, type (default is 'data' for td, pass 'header' for th)
// can include associative array of optional additional attributes
$tbl->addCell('Edit', '', 'header');
$tbl->addCell('Name', '', 'header');
$tbl->addCell('URL', '', 'header');
$tbl->addCell('Status', '', 'header');
$tbl->addCell('Hidden', '', 'header');
$tbl->addCell('Delete', '', 'header');

$tbl->addTSection('tbody');

$han = $DB->query ( "SELECT * FROM site_folder ORDER BY folder_order DESC" );
if ($han->rowCount ()) {
	while ( $ref = $han->fetch () ) {
		$tbl->addRow();
		$tbl->addCell( "<a href='folder_form.php?id=$ref->folder_id'>".$PAGE->getEditIcon()."</a>" );
		$tbl->addCell( $PAGE->getFolderIcon()." $ref->folder_name" );
		$tbl->addCell( "<a href='$CFG->webroot/$ref->folder_url'>$ref->folder_url</a>" );
		$tbl->addCell( $ref->folder_status );
		$tbl->addCell( $ref->folder_hidden );
		$tbl->addCell( "<a href='site_view.php?disolve_folder=$ref->folder_id'>".$PAGE->getDeleteIcon()."</a>" );
		$shan = $DB->query ( "SELECT * FROM site_page WHERE page_folder_id = ? ORDER BY page_order DESC", $ref->folder_id );
		if ($shan->rowCount ()) {
			while ( $sref = $shan->fetch () ) {
				$tbl->addRow();
				$tbl->addCell( "<a href='page_form.php?id=$sref->page_id'>".$PAGE->getEditIcon()."</a>" );
				$tbl->addCell( $PAGE->getSubPageIcon()." $sref->page_name" );
				$tbl->addCell( "<a href='$CFG->webroot/$ref->folder_url/$sref->page_url'>$ref->folder_url/$sref->page_url</a>" );
				$tbl->addCell( $sref->page_status );
				$tbl->addCell( $sref->page_hidden );
				$tbl->addCell( "<a href='site_view.php?disolve_page=$sref->page_id'>".$PAGE->getDeleteIcon()."</a>" );
			}
		}
	}
}else{
	$tbl->addRow();
	$tbl->addCell('No Page items found.', 'foot', 'data', array('colspan'=>6) );
}

echo '<div class="table-responsive">';
echo $tbl->display();
echo '</div>';


$tbl = new HTML_Table('', 'table table-bordered table-hover', 1, array('width' => '100%') );

$tbl->addHeader( 'Footer Menu' );

//$tbl->addTSection('thead');
$tbl->addRow();
// arguments: cell content, class, type (default is 'data' for td, pass 'header' for th)
// can include associative array of optional additional attributes
$tbl->addCell('Edit', '', 'header');
$tbl->addCell('Name', '', 'header');
$tbl->addCell('Delete', '', 'header');

$tbl->addTSection('tbody');

$han = $DB->query ( "SELECT * FROM site_footer ORDER BY footer_order DESC" );
if ($han->rowCount ()) {
	while ( $ref = $han->fetch () ) {
		$tbl->addRow();
		$tbl->addCell( "<a href='footer_form.php?id=$ref->footer_id'>".$PAGE->getEditIcon()."</a>" );
		$tbl->addCell( "$ref->footer_name" );
		$tbl->addCell( "<a href='site_view.php?disolve_footer=$ref->footer_id'>".$PAGE->getDeleteIcon()."</a>" );
	}
}else{
	$tbl->addRow();
	$tbl->addCell('No Footer items found.', 'foot', 'data', array('colspan'=>3) );
}

echo '<div class="table-responsive">';
echo $tbl->display();
echo '</div>';

// The Footer
include $CFG->adminserverroot.'/_includes/gui/footer.php';
?>