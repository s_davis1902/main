<?php
class BareTheme extends SitePage {
	public $theme='bare';
	public $menuFill='default';
	public $extraHeaderIcon='';
	public $showTitle = true;
	public $homepageOverlay = '';
	public $pageAlert = '';
	
	public function setMenuFill( $fill ){
		$this->menuFill = $fill;
	}
	
	public function setExtraHeaderIcon( $text ){
		$this->extraHeaderIcon = $text;	
	}
	
	public function setShowTitle( $show ){
		$this->showTitle = $show;	
	}
	
	public function showTitle(){
		return $this->showTitle;	
	}
	
	public function setHomepageOverlay( $overlay ){
		$this->homepageOverlay = $overlay;
	}
	
	public function getHomepageOverlay(){
		return $this->homepageOverlay;
	}
	
	public function setAlert( $message, $type = 'success', $dismissible = 1 ){
		in_array( $type, array( 'success', 'info', 'warning', 'danger' ) ) || $type == 'success';
 		$alert = "<div class='alert alert-$type";
 		$dismissible && $alert.= " alert-dismissible";
 		$alert.= "' role='alert'>";
 		$dismissible && $alert.= '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
 		$alert.="$message</div>";
 		
 		$_SESSION['themePageAlert'] = $alert;
 		$this->pageAlert = $alert;
 		
//  		echo "----".$_SESSION['themePageAlert'];
	}
	
	public function getAlert(){
		if( !$this->pageAlert && isset( $_SESSION['themePageAlert'] ) && $_SESSION['themePageAlert'] ){
			$this->pageAlert = $_SESSION['themePageAlert'];
		}
		$_SESSION['themePageAlert'] = '';
		return $this->pageAlert;	
	}
	
	public function siteMenu(){
		global $DB, $CFG;
	
		echo "<div class=\"nav-outer-stich $this->menuFill-fill\">&#160;</div>
		<div class=\"navigation $this->menuFill-fill\">
			<div class=\"container\">";
	
		$query = "SELECT * FROM site_folder
				WHERE folder_status='Active' AND folder_hidden='No'
				ORDER BY folder_order DESC";
		$fhan = $DB->query ( $query );
	
		if ($fhan->rowCount ()) {
			while ( $fref = $fhan->fetch () ) {
	
				$query2 = "SELECT * FROM site_page
						WHERE page_folder_id=? AND page_status='Active' AND page_hidden='No'
						ORDER BY page_order DESC";
				$phan = $DB->query ( $query2, $fref->folder_id );
	
				if ($phan->rowCount ()) {
					// no sub items, just one menu item
					$pref = $phan->fetch ();
					echo "<a href='" . $CFG->webroot . "/".$fref->folder_url."/" . $pref->page_url . "'>";
					echo $pref->page_name . "</a>\n";
				}
			}
		}
		echo "</div>
		</div>
		<div class=\"nav-outer-stich $this->menuFill-fill\">&#160;</div>\n";
	}
	
	public function siteLoginButton(){
		global $CFG;
		
		// if logged in
		if( isset( $CFG->front_username ) && isset( $CFG->front_profileurl ) && isset( $CFG->front_logouturl ) ){
			echo '<a href="'.$CFG->front_logouturl.'"><div class="container login-logout"><button type="button" class="btn btn-info">Logout</button></div></a>';
		}elseif( isset( $CFG->front_loginname ) && isset( $CFG->front_loginurl ) ){
			echo '<a href="'.$CFG->front_loginurl.'"><div class="container login-logout"><button type="button" class="btn btn-info">Login</button></div></a>';
		}
	}
	
	public function siteFooter(){
		global $DB, $CFG;

		$han = $DB->query('SELECT * FROM site_footer ORDER BY footer_order DESC');
		if ($han->rowCount ()) {
			while ( $ref = $han->fetch () ) {
				if( $ref->footer_url ){
					echo "<span><a href='$ref->footer_url'>$ref->footer_name</a></span>";
				}elseif( $ref->footer_page_id ){
					$page = $DB->getRecord('* FROM site_page p, site_folder f WHERE p.page_folder_id = f.folder_id AND p.page_id = ?', $ref->footer_page_id );
					if( $page ){
						echo "<span><a href='$CFG->webroot/$page->folder_url/$page->page_url'>$ref->footer_name</a></span>";
					}
				}
			}
		}
	}
}
?>