<!DOCTYPE html>
<html>
<head>
	<?php
    $pagetitle = $this->getPageTitle();
    echo "<title>$pagetitle</title>";
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<!-- jquery -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

	<!-- fashion theme -->
	<link rel="stylesheet" href="<?php echo $themeweburl?>/css/fashion.css">
	<link rel="stylesheet" href="<?php echo $themeweburl?>/css/bootstrap-tagsinput.css">

	
</head>
<body>

<!-- sticky footer wrapper -->
<div id="wrap">
	<!-- sticky footer content -->
	<div id="main">

		<!-- header with logo -->
		<div class="header">
		<?php $this->siteLoginButton(); ?>
			<a href="<?php echo $CFG->webroot;?>">
				<div class="afams-logo">
						<div class="container">African Fashion Model Search</div>
				</div>
			</a>
		</div>

		<!-- navigation -->
		<?php $this->siteMenu();?>
		<?php echo $this->getHomepageOverlay();?>
	
		<?php
		if( in_array( $pagetitle, array( 'About', 'Home', 'Contact Us' ) ) ){
		?>
		<div class="offset1 span8 pull-left" style="bottom:0;">
    		
    		
    		
    		
		
		
		
		
		
		
		
		
		<div class="row-fluid">
    <div class="span12" id="slider">
      <!-- Top part of the slider -->
      <div class="row-fluid">
        <div class="span8" id="carousel-bounding-box">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Carousel items -->
            <div class="carousel-inner">
              <div class="active item" data-slide-number="0"><img src="<?php echo $themeweburl?>/images/1Ad.jpg"></div>
              <div class="item" data-slide-number="1"><img src="<?php echo $themeweburl?>/images/2Ad.jpg"></div>
              <div class="item" data-slide-number="2"><img src="<?php echo $themeweburl?>/images/3Ad.jpg"></div>
              <div class="item" data-slide-number="3"><img src="<?php echo $themeweburl?>/images/4Ad.jpg"></div>
              <div class="item" data-slide-number="4"><img src="<?php echo $themeweburl?>/images/5Ad.jpg"></div>
            </div>
            <!-- Carousel nav -->
              <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left"></span>
			  </a>
			  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right"></span>
			  </a>
          </div>
        </div>
        
        
      </div>
      
    </div>
  </div>
		
		
		
		
		
		
		
    		
    		
    	</div>
    	<?php
		}
		?>

		<!-- page content -->
		<div class="container">
			<?php echo $this->extraHeaderIcon; ?>
			<?php
			if( $this->showTitle() ){
				echo "<h1 class=\"page-big-title\">$pagetitle</h1>";
			}
			?>
			<?php
				echo $this->getAlert();
			?>
				<?php echo $this->getContent();?>
			<hr/>

			

			<br/>
			<br/>

		</div>

	
	</div>	
</div>

<!-- sricky footer -->
<div class="footer">
	<div class="nav-outer-stich">&#160;</div>
	<div class="container">
		<div class="afams-logo">African Fashion Model Search</div>
		<div class="actlap">An ACTLAP Initiative</div>
		<div class="legal">
			<span>Copyright 2014 AFAMS</span>
			<?php $this->siteFooter();?>
		</div>
	</div>
</div>

</body>
</html>