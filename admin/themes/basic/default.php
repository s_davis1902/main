<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php
    $pagetitle = $this->getPageTitle();
    echo "<title>$pagetitle</title>";
	?>
	
    <!-- Bootstrap core CSS -->
    <link href="<?php echo $themeweburl?>/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $CFG->adminwebroot; ?>/_includes/font-awesome/css/font-awesome.min.css">

    <!-- Add custom CSS here -->
    <style>
	body {margin-top: 60px;}
	</style>
	
	<script src="//code.jquery.com/jquery-latest.min.js"></script>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
<!--           <a class="navbar-brand" href="index.html">Start Bootstrap</a> -->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
        <?php $this->siteMenu();?>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>

    <div class="container">

      <div class="row">
        <div class="col-lg-12">
          <h1><?php echo $pagetitle; ?></h1>
          <?php echo $this->getContent();?>
        </div>
      </div>

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Make sure to add jQuery - download the most recent version at http://jquery.com/ -->
    <script src="<?php echo $themeweburl?>/js/bootstrap.js"></script>
  </body>
</html>