<?php
// include main config
include('admin/config.php');

$request = $PAGE->getRequestUrl();


if( !$PAGE->processUrl( $request ) ){
	echo '404';
	exit(0);
}

$USER = $PAGE->checkModuleLogin();

$PAGE->processHeader();

$PAGE->outputPage();

?>
