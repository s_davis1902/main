<?php

use Phinx\Migration\AbstractMigration;

class CreateAdminMenu extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(){
        $table = $this->table('admin_menu');
        $table->addColumn('name', 'string', array('limit' => 100))
              ->addColumn('url', 'string', array('limit' => 100))
              ->addColumn('parent', 'integer')
              ->addColumn('order', 'integer')
              ->addColumn('location', 'enum', array('values' => 'Top,Side', 'default' => 'Side'))
              ->addColumn('faicon', 'string', array('limit' => 50))
              ->addColumn('admin', 'enum', array('values' => 'Yes,No'))
              ->save();

        // insert data
        $columns = ['id', 'name', 'url', 'parent', 'order', 'location', 'faicon', 'admin'];
        $table = $this->table('admin_menu');

        $data = [
            ['1', 'Site', '', '0', '20', 'Side', 'home', 'No'],
            ['2', 'Site View', 'site/site_view.php', '1', '20', 'Side', 'home', 'No'],
            ['3', 'Admin', '', '0', '15', 'Top', 'gear', 'Yes'],
            ['4', 'Users', 'core/user_list.php', '3', '20', 'Top', 'user', 'Yes'],
            ['5', 'Menu', 'core/menu_list.php', '3', '16', 'Top', 'table', 'Yes'],
            ['6', 'Modules', 'core/module_list.php', '3', '6', 'Top', 'cogs', 'Yes'],
            ['7', 'Site Config', 'core/config.php', '3', '5', 'Top', 'crosshairs', 'Yes']
        ];
        $table->insert($columns, $data);
        $table->saveData(); 
    }

    public function down(){
        $this->dropTable('admin_menu');
    }
}
