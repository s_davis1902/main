<?php

use Phinx\Migration\AbstractMigration;

class CreateConfig extends AbstractMigration
{
    public function up(){
        $table = $this->table('config');
        $table->addColumn('area', 'string', array('limit' => 50))
              ->addColumn('field', 'string', array('limit' => 50))
              ->addColumn('value', 'text')
              ->save();
    }   

    public function down(){
        $this->dropTable('config');
    }
}
