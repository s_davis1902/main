<?php

use Phinx\Migration\AbstractMigration;

class CreateModule extends AbstractMigration
{
    public function up(){
        $table = $this->table('module');
        $table->addColumn('name', 'string', array('limit' => 100))
              ->addColumn('location', 'string', array('limit' => 100))
              ->addColumn('status', 'enum', array( 'values' => 'Active,Inactive', 'default' => 'Active' ))
              ->addColumn('login', 'enum', array( 'values' => 'Yes,No', 'default' => 'No'))
              ->save();
    }   

    public function down(){
        $this->dropTable('module');
    }
}
