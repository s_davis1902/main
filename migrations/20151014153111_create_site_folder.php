<?php

use Phinx\Migration\AbstractMigration;

class CreateSiteFolder extends AbstractMigration
{
    public function up(){
        $table = $this->table('site_folder');
        $table->addColumn('name', 'string', array('limit' => 50))
              ->addColumn('url', 'string', array('limit' => 50))
              ->addColumn('order', 'integer')
              ->addColumn('hidden', 'enum', array('values' => 'Yes,No', 'default' => 'Yes'))
              ->addColumn('status', 'enum', array('values' => 'Active,Inactive', 'default' => 'Active'))
              ->save();
    }   

    public function down(){
        $this->dropTable('site_folder');
    }
}
