<?php

use Phinx\Migration\AbstractMigration;

class CreateSiteFooter extends AbstractMigration
{
    public function up(){
        $table = $this->table('site_footer');
        $table->addColumn('name', 'string', array('limit' => 50))
              ->addColumn('url', 'string', array('limit' => 50))
              ->addColumn('page_id', 'integer')
              ->addColumn('order', 'integer')
              ->addColumn('status', 'enum', array('values' => 'Active,Inactive', 'default' => 'Active'))
              ->save();
    }   

    public function down(){
        $this->dropTable('site_footer');
    }
}
