<?php

use Phinx\Migration\AbstractMigration;

class CreateSitePage extends AbstractMigration
{
    public function up(){
        $table = $this->table('site_page');
        $table->addColumn('folder_id', 'integer')
              ->addColumn('module_id', 'integer')
              ->addColumn('name', 'string', array('limit' => 50))
              ->addColumn('url', 'string', array('limit' => 50))
              ->addColumn('order', 'integer')
              ->addColumn('status', 'enum', array('values' => 'Active,Inactive', 'default' => 'Active'))
              ->addColumn('hidden', 'enum', array('values' => 'Yes,No', 'default' => 'Yes'))
              ->addColumn('content', 'text')
              ->addColumn('redirect', 'string', array('limit' => 100))
              ->save();
    }   

    public function down(){
        $this->dropTable('site_page');
    }
}
